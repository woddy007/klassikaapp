import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity
} from 'react-native';

class Button extends Component{

    getClassButton = () => {
        const type = this.props.type
        let classButton = []

        if ( type != 'link' ){
            classButton = [ styles[type]['button'], styles['button']['button'] ]
        }else{
            classButton = [ styles[type]['button'] ]
        }

        return classButton
    }
    getClassText = () => {
        const type = this.props.type
        let classText = []

        if ( type != 'link' ){
            classText = [ styles[type]['text'], styles['button']['text'] ]
        }else{
            classText = [ styles[type]['text'] ]
        }

        return classText
    }
    clickButton = () => {
        if ( this.props.click ){
            this.props.click()
        }
    }

    render() {

        return (
            <TouchableOpacity
                style={this.getClassButton()}
                onPress={() => this.clickButton()}
            >
                <Text style={this.getClassText()}>{this.props.label}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = {
    primary: {
        button: {
            backgroundColor: '#8F7E5A',
        },
        text: {
            color: '#FFFFFF',
        },
    },
    secondary: {
        button: {
            backgroundColor: '#000000'
        },
        text: {
            color: 'white'
        },
    },
    outline: {
        button: {
            backgroundColor: 'transparent',
            borderStyle: 'solid',
            borderWidth: 1,
            borderColor: '#000000'
        },
        text: {},
    },
    link: {
        button: {
            fontSize: 12,
            borderBottomWidth: 1,
            borderStyle: 'solid',
            borderColor: '#000000'
        },
        text: {},
    },
    button: {
        button: {
            height: 35,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center'
        },
        text: {
            fontSize: 12,
            textTransform: 'uppercase'
        },
    }
}

export default Button
