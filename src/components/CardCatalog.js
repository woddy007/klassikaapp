import React, {PureComponent} from 'react'
import {
    StyleSheet,
    View,
    Text,
    Button,
    Image, TouchableOpacity
} from 'react-native';

import axios from "../plugins/axios";

const percentIcon = require('../../assets/icons/percent.png')
const wishFullIcon = require('../../assets/icons/heartFull.png')
const wishEmptyIcon = require('../../assets/icons/heart.png')

class CardCatalog extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            favouritActive: false,
        }
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        let check = prevProps.props.chechFavourit(prevProps.item.id)

        if ( prevState.favouritActive != check ){
            this.setState({
                favouritActive: check
            })
        }
    }

    clickCard = () => {
        const {navigate} = this.props.props.navigation;

        navigate('Product', {
            productId: this.props.item.id
        });
    }
    productFavourites = () => {
        let id = this.props.item.id

        this.setState({
            favouritActive: !this.state.favouritActive
        })

        axios('get', 'api-shop-shopapp/product?id=' + id).then(response => {
            this.props.isAddFavourites( response.data )
        }).catch(error => {
            this.setState({
                favouritActive: false
            })
        })
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.clickCard}
                style={styles.card}
            >
                <View style={styles.imageContent}>
                    <View style={styles.contentWish}>
                        <TouchableOpacity
                            onPress={() => this.productFavourites()}
                            style={styles.buttonWish}
                        >
                            <Image
                                source={(this.state.favouritActive)? wishFullIcon: wishEmptyIcon}
                                style={styles.imageWish}
                            />
                        </TouchableOpacity>
                    </View>

                    <Image
                        style={styles.image}
                        source={{uri: this.props.item.image}}
                        resizeMode='contain'
                    />
                    {
                        (this.props.item.price_old) ?
                            <View style={styles.discountContent}>
                                <Image
                                    style={styles.discount}
                                    source={percentIcon}
                                />
                            </View>
                            :
                            <View></View>
                    }
                </View>
                <View style={styles.info}>
                    <Text style={styles.title}>{this.props.item.name}</Text>
                    <View style={styles.prices}>
                        <Text style={styles.price}>{this.props.item.price_new} ₽</Text>
                        {
                            (this.props.item.price_old)? <Text style={styles.priceOld}>{this.props.item.price_old} ₽</Text> : <View></View>
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        width: '50%',
        paddingLeft: 8,
        marginBottom: 25
    },
    imageContent: {
        flex: 1,
        marginBottom: 19,
        height: 140
    },
    info: {},
    image: {
        flex: 1,
    },
    brand: {
        fontSize: 11,
        marginBottom: 3,
        color: '#989898',
        fontWeight: '300'
    },
    title: {
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 3
    },
    price: {
        fontSize: 12,
        fontWeight: '500',
        color: '#EF5252',
        marginRight: 5
    },
    prices: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    priceOld: {
        fontSize: 10,
        color: '#959595',
    },

    discount: {
        width: 12,
        height: 12,
        tintColor: 'white'
    },
    discountContent: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: 40,
        height: 20,
        backgroundColor: '#EF5252',
        justifyContent: 'center',
        alignItems: 'center'
    },

    contentWish: {
        position: 'absolute',
        left: 8,
        top: 8,
        zIndex: 5
    },
    imageWish: {
        width: 25,
        height: 25,
        tintColor: '#EF5252',
    }
})

export default CardCatalog
