import React, {Component} from "react";
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

const styles = StyleSheet.create({
    content: {
        paddingLeft: 8,
        height: 200,
    },
    card: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
        marginBottom: 8,
        padding: 17
    },
    imageContent: {
        flex: 1,
        width: '100%',
        paddingBottom: 10,
    },
    image: {
        resizeMode: 'contain',
        width: '100%',
        height: '100%'
    },
    title: {
        fontSize: 14,
        color: '#000000',
        textTransform: 'uppercase'
    }
});

class CardCategory extends Component {

    clickCard = () => {
        const {navigate} = this.props.props.navigation;

        navigate('Catalog', {
            categoryId: this.props.item.id,
            categotyName: this.props.item.name
        });
    }

    render() {
        let title = this.props.item.name
        let width = this.props.width

        return (
            <TouchableOpacity
                onPress={this.clickCard}
                style={[styles.content, {width: width}]}
            >
                <View
                    style={styles.card}
                >
                    <View style={styles.imageContent}>
                        <Image
                            source={this.props.item.image}
                            style={styles.image}
                        />
                    </View>
                    <Text
                        style={styles.title}
                    >
                        {title}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export default CardCategory
