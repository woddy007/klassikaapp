// https://kmagiera.github.io/react-native-gesture-handler/docs/component-swipeable.html#example

import React, {Component} from "react";
import {
    View,
    Image,
    Text,
    StyleSheet,
    Button,
    TouchableOpacity, Alert, Animated
} from 'react-native';
import colors from '../styles/colors'
import Select from '../components/Select'
import Swipeable from 'react-native-gesture-handler/Swipeable';
import RNPickerSelect from "react-native-picker-select";
import {RectButton} from "react-native-gesture-handler";
import {DropDownHolder} from "../components/DropDownAlert";

const iconClose = require('../../assets/icons/close.png');
const iconArrow = require('../../assets/icons/next-right.png');
const iconDelete = require('../../assets/icons/delete.png');

class CardFavorites extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modification: null,
        };

        this.refSwipeable = React.createRef();
    }

    deleteProduct = () => {
        this.refSwipeable.current.close()

        this.props.isDelete(this.props.product)
    }
    getVariants = ( item ) => {
        let list = []

        console.log('item: ', item)

        if ( item ){
            item.variants.map(variant => {
                list.push({
                    label: variant.label,
                    value: variant.id
                })
            })
        }


        return list
    }
    changeModification = (id) => {
        this.setState({
            modification: id
        })
    }
    productToCart = () => {
        if (!this.state.modification && this.props.product.variants.length > 0) {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не выбран размер');
        } else {
            let data = {
                product: this.props.product,
                modification: this.state.modification
            }

            this.setState({
                modification: null
            })

            this.props.isToCart(data)

            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Вы добавили товар в корзину');
        }
    }

    renderRightAction = (x, progress) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0],
        });
        return (
            <Animated.View style={{flex: 1, transform: [{translateX: trans}]}}>
                <RectButton
                    style={[styles.rightAction, {backgroundColor: '#EF5252'}]}
                    onPress={() => this.deleteProduct()}
                >
                    <Image
                        style={{width: 25, height: 25, tintColor: 'white'}}
                        source={iconDelete}
                    />
                </RectButton>
            </Animated.View>
        );
    };
    renderRightActions = (progress, dragX) => {
        return (
            <View style={{width: 80, flexDirection: 'row'}}>
                {this.renderRightAction(80, progress)}
            </View>
        )
    }

    render() {
        let product = this.props.product

        return (
            <Swipeable
                ref={this.refSwipeable}
                renderRightActions={this.renderRightActions}
                overshootRight={false}
                friction={2}
            >
                <View style={styles.card}>
                    <View style={styles.topLine}>
                        <View style={styles.imageContent}>
                            <Image
                                style={styles.image}
                                source={{uri: product.photos[0]['src']}}
                            />
                        </View>
                        <View style={{paddingLeft: 13, width: '70%'}}>
                            <View style={styles.topLineRight}>
                                {(product.brand) && <Text style={styles.brand}>{ product.brand.name }</Text>}
                                <Image
                                    style={styles.iconArrow}
                                    source={iconArrow}
                                />
                            </View>
                            <Text style={styles.title}>{ product.name }</Text>
                            <View style={styles.vendorСode}>
                                <Text style={styles.vendorСodeText}>Артикул:</Text>
                                <Text style={styles.vendorСodeValue}>{ product.code }</Text>
                            </View>
                            <View style={styles.prices}>
                                <Text style={styles.priceNew}>{product.price} ₽</Text>
                                {
                                    (product.price_old) ?
                                        <Text style={styles.priceOld}>{product.price_old} ₽ (-{ product.sale }%)</Text> :
                                        <View></View>
                                }
                            </View>
                        </View>
                    </View>
                    <View style={styles.bottomLine}>
                        {(this.getVariants(product.variants[0]).length > 0) && <View style={styles.bottomLineItem}>
                            <View style={styles.pickerContainer}>
                                <RNPickerSelect
                                    onValueChange={(id) => this.changeModification(id)}
                                    placeholder={{ label: 'Размер' }}
                                    style={{ ...picker, }}
                                    value={this.state.modification}
                                    items={this.getVariants(product.variants[0])}
                                />
                            </View>
                        </View>}
                        <View style={styles.bottomLineItem}>
                            <TouchableOpacity
                                onPress={() => this.productToCart()}
                                style={styles.buttonAdd}
                            >
                                <Text style={styles.buttonAddLabel}>В КОРЗИНУ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Swipeable>
        )
    }
}

const picker = StyleSheet.create({
    inputIOS: {
        height: 35,
        color: '#000000',
    },
    inputAndroid: {
        height: 35,
        color: '#000000',
    }
});

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'row',
        padding: 16,
        backgroundColor: 'white',
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(205, 205, 205, 0.5)',
        flexDirection: 'column',
    },
    topLine: {
        flex: 1,
        flexWrap: 'nowrap',
        flexDirection: 'row',
        minHeight: 92,
        marginBottom: 10
    },
    bottomLine: {
        flex: 1,
        flexWrap: 'nowrap',
        flexDirection: 'row',
        marginLeft: -8
    },
    bottomLineItem: {
        paddingLeft: 8,
        width: '50%',
        marginLeft: 'auto'
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
    },
    imageContent: {
        flex: 1,
        width: '30%',
    },
    brand: {
        color: '#6A6A6A',
        fontSize: 12,
        fontWeight: '300',
        marginBottom: 2
    },
    title: {
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 3
    },
    price: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#EF5252'
    },
    buttonAdd: {
        backgroundColor: colors.primary,
        paddingHorizontal: 8,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 35
    },
    buttonAddLabel: {
        color: 'white',
        fontSize: 10,
        fontWeight: '500'
    },
    vendorСode: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 8
    },
    vendorСodeText: {
        marginRight: 8,
        fontSize: 12,
        fontWeight: '300'
    },
    vendorСodeValue: {
        fontSize: 12,
        fontWeight: '300'
    },
    prices: {
        flexDirection: 'row',
        marginBottom: 17,
        alignItems: 'flex-end'
    },
    priceNew: {
        fontSize: 16,
        color: '#EF5252',
        marginRight: 7,
        fontWeight: '500'
    },
    priceOld: {
        fontSize: 12,
        color: '#959595'
    },

    topLineRight: {
        marginBottom: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    iconArrow: {
        width: 12,
        height: 12,
        tintColor: '#7C7C7C',
        marginLeft: 'auto'
    },

    pickerContainer: {
        borderColor: 'rgba(205, 205, 205, 0.3)',
        borderStyle: 'solid',
        borderWidth: 1,
        flex: 1,
        height: 35,
    },
    contentPickers: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 5
    },
    stylePicker: {
        flex: 1,
    },
    stylePickerText: {
        fontSize: 8
    },

    swipeable: {
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EF5252'
    },
    actionIcon: {
        width: 30,
        marginHorizontal: 10
    },
    rightAction: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    }
});

export default CardFavorites
