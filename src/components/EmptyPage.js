import React, {Component} from 'react'
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";


class EmptyPage extends Component {
    render() {

        let title = this.props.title
        let text = this.props.text
        let textButton = this.props.textButton
        let link = this.props.link

        return (
            <View style={styles.block}>
                <Text style={styles.title}>{ title }</Text>
                <Text style={styles.text}>{ text }</Text>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText}>{ textButton }</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    block: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 'auto'
    },
    title: {
        fontSize: 18,
        marginBottom: 14,
        color: '#000000',
        fontWeight: '500'
    },
    text: {
        fontSize: 14,
        color: '#7C7C7C',
        marginBottom: 25
    },
    button: {
        paddingHorizontal: 16,
        paddingVertical: 11,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.3)'
    },
    buttonText: {
        fontSize: 12,
        fontWeight: '500'
    }
});

export default EmptyPage
