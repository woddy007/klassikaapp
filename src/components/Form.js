import React, {Component} from 'react'
import {
    View,
} from 'react-native';

class Form extends Component {
    checkForm = () => {
        let bool = true
        let childrens = this.props.children
        let list = []

        if (!childrens.length){
            list.push(childrens)
        }else{
            list = Object.values(childrens)
        }


        list.map(children => {
            let props = children.props

            let value = props.value
            let validation = props.rules(value)

            if ( validation != true ){
                bool = false
            }
        })

        return bool
    }

    render() {
        return(
            <View>
                { this.props.children }
            </View>
        )
    }
}

export default Form
