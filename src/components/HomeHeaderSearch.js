import React, {Component} from "react";
import {View, Text, TextInput, StyleSheet, TouchableOpacity, Image} from 'react-native'

const iconUser = require('../../assets/icons/search.png');

class HomeHeaderSearch extends Component {
    constructor(props) {
        super(props)

        this.state = {
            value: null
        }
    }

    onSearch = () => {

    }

    render() {
        const inputAccessoryViewID = "uniqueID";
        return (
            <View style={styles.block}>
                <TextInput
                    style={styles.input}
                    inputAccessoryViewID={inputAccessoryViewID}
                    value={this.state.value}
                    onChangeText={value => this.setState({value})}
                    placeholder='Искать товар'
                />
                <TouchableOpacity
                    onPress={this.onSearch}
                    style={styles.searchButton}
                >
                    <Image
                        resizeMode="contain"
                        source={iconUser}
                        style={styles.searchIcon}
                    />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    block: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 16,
    },
    input: {
        height: 35,
        flex: 1,
        backgroundColor: '#F3F5F7',
        paddingHorizontal: 15,
    },
    searchButton: {
        position: 'absolute',
        right: 31,
        top: 11
    },
    searchIcon: {
        width: 15,
        height: 15,
        tintColor: '#959595',
    }
})

export default HomeHeaderSearch
