import React, {Component} from "react";
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';

const iconHide = require('../../assets/icons/hide.png');
const iconView = require('../../assets/icons/view.png');

class InputText extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
            showError: false,
            secureTextEntry: false
        }
    }

    componentDidMount() {
        this.setState({
            value: this.props.value,
            secureTextEntry: this.props.type == 'password'
        })
    }
    onChangeText = (value) => {
        this.props.onChange(value)
        this.setState({value, showError: true})
    }
    checkValue = () => {
        this.setState({ showError: true })
    }
    changeTypeText = () => {
        this.setState({ secureTextEntry: !this.state.secureTextEntry })
    }
    getClassInput = () => {
        let classInput = [styles.input]

        if ( this.props.filled ){
            classInput.push(styles.filledInput)
        }

        if ( this.props.styleInput ){
            classInput.push(this.props.styleInput)
        }

        return classInput
    }
    getClassLabel = () => {
        let classLabel = [styles.label]

        if ( this.props.filled ){
            classLabel.push(styles.filledLabel)
        }

        return classLabel
    }
    getClassContent = () => {
        let classContent = [styles.block]
        if ( this.props.classContent ){
            classContent = classContent.concat( this.props.classContent )
        }

        return classContent
    }

    render() {
        const inputAccessoryViewID = "uniqueID";

        return (
            <View style={this.getClassContent()}>
                <Text style={this.getClassLabel()}>{this.props.label}</Text>
                <View style={styles.inputContent}>
                    <TextInput
                        style={this.getClassInput()}
                        inputAccessoryViewID={inputAccessoryViewID}
                        onChangeText={value => this.onChangeText(value)}
                        value={this.state.value}
                        placeholder={(this.props.placeholder)? this.props.placeholder: this.props.label}
                        secureTextEntry={ this.state.secureTextEntry }
                        keyboardType={ (this.props.keyboardType)? this.props.keyboardType: 'default' }
                        editable={!this.props.disabled}
                    />

                    {
                        ( this.props.type == 'password' )? <TouchableOpacity
                            onPress={this.changeTypeText}
                            style={styles.changeTypeButton}
                        >
                            <Image
                                resizeMode="contain"
                                source={(this.state.secureTextEntry)? iconView : iconHide }
                                style={styles.changeTypeIcon}
                            />
                        </TouchableOpacity> : <View/>
                    }
                </View>
                {
                    (this.state.showError && this.props.rules && (this.props.rules(this.state.value) != true)) ?
                        <Text style={styles.textError}>{this.props.rules(this.state.value)}</Text>
                        :
                        <View/>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    block: {},
    label: {
        fontSize: 13,
        marginBottom: 6,
    },
    input: {
        height: 35,
        width: '100%',
        backgroundColor: 'white',
        borderWidth: 0.5,
        paddingHorizontal: 15,
        borderStyle: 'solid',
        borderColor: '#CDCDCD'
    },
    textError: {
        color: '#EF5252',
        fontSize: 10,
        position: 'absolute',
        bottom: -15
    },

    filledInput: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        borderBottomWidth: 0.5,
        borderStyle: 'solid',
        borderColor: '#CDCDCD',
        paddingHorizontal: 0
    },
    filledLabel: {
        marginBottom: 0,
        color: '#7C7C7C'
    },

    changeTypeIcon: {
        width: 20,
        height: 20
    },
    changeTypeButton: {
        position: 'absolute',
        right: 7,
        top: 0,
        zIndex: 5,
        padding: 7,
    },
    inputContent: {
        flexDirection: 'row',
    },
})

export default InputText
