import React, {Component} from 'react'
import {
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Modal,
    Image
} from 'react-native';
import colors from '../styles/colors'

const logo = require('../../assets/logo.png');

class Loading extends Component {

    render() {
        return (
            <Modal
                supportedOrientations={['landscape', 'portrait']}
                transparent
                visible={this.props.open}
            >
                <View style={styles.content}>
                    <ActivityIndicator
                        size="large"
                        color={colors.primary}
                    />
                    <Text style={styles.text}>Идет загрузка...</Text>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: '#F3F5F7',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 18,
        textAlign: 'center'
    },
    loading: {
        width: 60,
        height: 60
    }
})

export default Loading
