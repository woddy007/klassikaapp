import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native'
import colors from '../styles/colors'

class Pagination extends Component {

    back = () => {}
    forward = () => {}
    toPage = (page) => {
        if ( page != this.props.page ){
            this.props.updatePage(page)
        }
    }
    getListPagination = () => {
        let totalPage = this.props.totalPage
        let page = this.props.page
        let list = []


        if (totalPage > 1 && totalPage <= 7) {
            for (let pageIndex = 1; pageIndex != totalPage + 1; pageIndex++) {
                list.push({
                    page: pageIndex,
                    title: pageIndex
                })
            }
        } else {
            for (let pageIndex = 1; pageIndex != totalPage; pageIndex++) {

            }

            // if (page != 1){
            //     list.push({
            //         page: 1,
            //         title: 1
            //     })
            // }
            // if (page > 4) {
            //     list.push({
            //         page: page - 2,
            //         title: '...'
            //     })
            // }
            // if (page > 3){
            //     list.push({
            //         page: page - 1,
            //         title: page - 1
            //     })
            // }
            // list.push({
            //     page: page,
            //     title: page
            // })
            // list.push({
            //     page: page + 1,
            //     title: page + 1
            // })
            // if (page < totalPage - 4) {
            //     list.push({
            //         page: page + 2,
            //         title: '...'
            //     })
            // }
            // list.push({
            //     page: totalPage,
            //     title: totalPage
            // })
        }


        return list
    }

    render() {
        let page = this.props.page

        return (
            <View style={styles.content}>
                {
                    this.getListPagination().map((item, idx) => {
                        return (
                            <TouchableOpacity
                                key={idx}
                                style={[styles.button, (page == item.page) ? styles.buttonActive : '']}
                                onPress={() => this.toPage(item.page)}
                            >
                                <Text style={styles.buttonText}>{item.title}</Text>
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: -5,
        marginBottom: 25
    },
    button: {
        width: 28,
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.primaryLight,
        marginRight: 5,
        borderRadius: 5
    },
    buttonActive: {
        backgroundColor: colors.primary
    },
    buttonText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '500'
    }
})

export default Pagination
