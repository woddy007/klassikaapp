//https://github.com/lawnstarter/react-native-picker-select - native select

import React, { Component } from "react";
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';
import {
    View,
    StyleSheet,
} from 'react-native';
import {colors} from "../styles";

class Select extends Component{
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }

    onChangePicker = (value, index) => {

    }

    render() {

        let placeholder = 'Выберите'

        return(
            <View style={styles.select}>
                <RNPickerSelect
                    onValueChange={(value) => console.log(value)}
                    placeholder={{ label: placeholder }}
                    style={{...picker, }}
                    items={[
                        { label: 'Football', value: 'football' },
                        { label: 'Baseball', value: 'baseball' },
                        { label: 'Hockey', value: 'hockey' },
                    ]}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    select: {
        backgroundColor: '#FFFFFF',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#CDCDCD'
    }
});

const picker = StyleSheet.create({
    inputIOS: {
        height: 32,
        padding: 7,
        color: '#000000',
        fontSize: 12
    },
    inputAndroid: {
        height: 32,
        padding: 7,
        color: '#000000',
        fontSize: 12
    }
});

export default Select
