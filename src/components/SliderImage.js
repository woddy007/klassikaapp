//https://www.npmjs.com/package/react-native-image-slider-box

import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Image,
    Modal,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text
} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import ImageView from 'react-native-image-view';

const arrowBottom = require('../../assets/icons/arrow-bottom.png');


class SliderImage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            backHandler: null,
            modalSlider: false
        }
    }

    componentDidMount = () => {

    }

    handleBackPress = () => {
        this.setState({modalSlider: false})
        this.backHandler = BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
    openModalSlider = (index) => {
        this.setState({modalSlider: true})
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    _renderComponent = (data) => {
        return (
            <TouchableOpacity
                onPress={(data) => this.openModalSlider(data)}
            >
                <Image
                    style={styles.image}
                    resizeMode='contain'
                    source={{uri: data.source.uri.src}}
                />
            </TouchableOpacity>
        )
    }
    _renderComponentModal = (data) => {
        return (
            <View>
                <Image
                    style={styles.imageModal}
                    resizeMode='contain'
                    source={{uri: data.source.uri.src}}
                />
            </View>
        )
    }

    getImagesModal = () => {
        const images = [];

        this.props.photos.map(item => {
            images.push({
                source: {
                    uri: item.src,
                }
            })
        })

        return images
    }

    render() {
        return (
            <View>
                <SliderBox
                    images={this.props.photos}
                    sliderBoxHeight={260}
                    dotColor="#EF5252"
                    circleLoop
                    ImageComponent={this._renderComponent}
                />

                <ImageView
                    images={this.getImagesModal()}
                    imageIndex={0}
                    isVisible={this.state.modalSlider}
                    glideAlways
                    isSwipeCloseEnabled
                    glideAlwaysDelay={150}
                    onClose={() => {
                        this.setState({modalSlider: false})
                    }}
                />

                {/*<Modal*/}
                {/*    animationType="slide"*/}
                {/*    transparent={false}*/}
                {/*    visible={this.state.modalSlider}*/}
                {/*    onRequestClose={() => this.setState({modalSlider: false})}*/}
                {/*    presentationStyle="fullScreen"*/}
                {/*>*/}
                {/*    <View style={styles.modalContent}>*/}
                {/*        <View style={styles.modalBar}>*/}
                {/*            <TouchableOpacity*/}
                {/*                onPress={() => { this.setState({modalSlider: false}) }}*/}
                {/*            >*/}
                {/*                <Image*/}
                {/*                    resizeMode="contain"*/}
                {/*                    source={arrowBottom}*/}
                {/*                    style={{ width: 20, height: 20 }}*/}
                {/*                />*/}
                {/*            </TouchableOpacity>*/}
                {/*        </View>*/}
                {/*        <SliderBox*/}
                {/*            images={this.props.photos}*/}
                {/*            dotColor="#EF5252"*/}
                {/*            circleLoop*/}
                {/*            ImageComponent={this._renderComponentModal}*/}
                {/*        />*/}
                {/*    </View>*/}
                {/*</Modal>*/}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 260,
    },
    imageModal: {
        height: '100%',
        width: '100%',
        maxWidth: '100%',
        maxHeight: '100%',
    },

    modalContent: {
        flex: 1,
        backgroundColor: '#F3F5F7',
    },
    modalBar: {
        height: 25,
        padding: 10,
        width: '100%',
        backgroundColor: '#F3F5F7',
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default SliderImage
