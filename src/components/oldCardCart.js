// https://kmagiera.github.io/react-native-gesture-handler/docs/component-swipeable.html#example

import React, {Component} from "react";
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    Button,
    Picker,
    TextInput,
    Alert,
    Animated
} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {RectButton} from 'react-native-gesture-handler';
import RNPickerSelect from "react-native-picker-select";
import Icon from 'react-native-vector-icons/MaterialIcons';

const iconClose = require('../../assets/icons/close.png');
const iconArrow = require('../../assets/icons/next-right.png');
const iconDelete = require('../../assets/icons/delete.png');

const AnimatedIcon = Animated.createAnimatedComponent(Icon);

class CardProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
            modification: this.props.product.modification,
            count: this.props.product.count,
        }

        this.refSwipeable = React.createRef();
    }

    deleteProduct = () => {
        this.refSwipeable.current.close()
        this.props.isDelete(this.props.product)
    }
    changeModification = (id) => {
        if (id != this.state.modification) {

            this.setState({
                modification: id
            })

            this.props.isChangeModification({
                product: this.props.product,
                newModification: id
            })
        }
    }
    getVariants = (item) => {
        let list = []

        if (item.variants.length == 0) {
            return list
        }

        item.variants.map(variant => {
            list.push({
                label: variant.label,
                value: variant.id
            })
        })


        return list
    }

    changeCount = ({value, type}) => {
        let oldCount = Number(value)
        let count = (type == 'add') ? oldCount + 1 : oldCount - 1

        if (count < 1) {
            Alert.alert(
                'Предупреждение',
                'Количество товара не может быть меньше 1'
            );

            count++
        }

        this.props.isChangeCount({
            product: this.props.product,
            count
        })
    }

    renderRightAction = (x, progress) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0],
        });
        return (
            <Animated.View style={{flex: 1, transform: [{translateX: trans}]}}>
                <RectButton
                    style={[styles.rightAction, {backgroundColor: '#EF5252'}]}
                    onPress={() => this.deleteProduct()}
                >
                    <Image
                        style={{width: 25, height: 25, tintColor: 'white'}}
                        source={iconDelete}
                    />
                </RectButton>
            </Animated.View>
        );
    };
    renderRightActions = (progress, dragX) => {
        return (
            <View style={{width: 80, flexDirection: 'row'}}>
                {this.renderRightAction(80, progress)}
            </View>
        )
    }

    render() {
        let {product, count, modification} = this.props.product

        return (
            <Swipeable
                ref={this.refSwipeable}
                renderRightActions={this.renderRightActions}
                overshootRight={false}
                friction={2}
            >
                <View style={styles.card}>
                    <View style={styles.imageContent}>
                        <Image
                            style={styles.image}
                            source={{uri: product.photos[0]['src']}}
                        />
                    </View>
                    <View style={styles.infoContent}>
                        <View style={styles.topLine}>
                            <Text style={styles.brand}>{product.brand.name}</Text>

                            <Image
                                style={styles.iconArrow}
                                source={iconArrow}
                            />
                        </View>
                        <Text style={styles.title}>{modification}</Text>
                        <Text style={styles.title}>{product.name}</Text>
                        <View style={styles.vendorСode}>
                            <Text style={styles.vendorСodeText}>Артикул:</Text>
                            <Text style={styles.vendorСodeValue}>{product.code}</Text>
                        </View>
                        <View style={styles.contentPickers}>
                            {
                                (product.variants.length > 0)
                                    ?
                                    <View style={styles.pickerContainer}>
                                        <RNPickerSelect
                                            onValueChange={(id) => {
                                                (id) ? this.changeModification(id) : ''
                                            }}
                                            placeholder={{label: 'Размер'}}
                                            style={{...picker,}}
                                            value={modification}
                                            items={this.getVariants(product.variants[0])}
                                        />
                                    </View>
                                    :
                                    <View/>
                            }
                            <View style={styles.countContainer}>
                                <TouchableOpacity
                                    style={styles.countButton}
                                    onPress={() => this.changeCount({value: count, type: 'subtraction'})}
                                >
                                    <Text style={styles.countButtonText}>-</Text>
                                </TouchableOpacity>
                                <TextInput
                                    style={styles.countInput}
                                    onChangeText={value => this.changeCount(value)}
                                    value={String(count)}
                                    keyboardType='number-pad'
                                />
                                <TouchableOpacity
                                    style={styles.countButton}
                                    onPress={() => this.changeCount({value: count, type: 'add'})}
                                >
                                    <Text style={styles.countButtonText}>+</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.prices}>
                            <Text style={styles.priceNew}>{product.price} ₽</Text>
                            {
                                (product.price_old) ?
                                    <Text style={styles.priceOld}>{product.price_old} ₽ (-{product.sale}%)</Text> :
                                    <View></View>
                            }
                        </View>
                    </View>
                </View>
            </Swipeable>
        )
    }
}

const picker = StyleSheet.create({
    inputIOS: {
        height: 30,
        color: '#000000',
    },
    inputAndroid: {
        height: 30,
        color: '#000000',
    }
});

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'row',
        padding: 16,
        backgroundColor: 'white',
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(205, 205, 205, 0.5)'
    },
    imageContent: {
        flex: 1,
        width: '30%'
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
    },
    infoContent: {
        width: '70%',
        paddingLeft: 13
    },
    brand: {
        color: '#6A6A6A',
        fontSize: 12,
        fontWeight: '300',
    },
    title: {
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 3
    },
    vendorСode: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 8
    },
    vendorСodeText: {
        marginRight: 8,
        fontSize: 12,
        fontWeight: '300'
    },
    vendorСodeValue: {
        fontSize: 12,
        fontWeight: '300'
    },
    selects: {},
    price: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#EF5252'
    },
    topLine: {
        marginBottom: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    iconArrow: {
        width: 12,
        height: 12,
        tintColor: '#7C7C7C',
    },
    prices: {
        flexDirection: 'row',
        marginBottom: 17,
        alignItems: 'flex-end'
    },
    priceNew: {
        fontSize: 16,
        color: '#EF5252',
        marginRight: 7,
        fontWeight: '500'
    },
    priceOld: {
        fontSize: 12,
        color: '#959595'
    },

    pickerContainer: {
        borderColor: 'rgba(205, 205, 205, 0.3)',
        borderStyle: 'solid',
        borderWidth: 1,
        flex: 1,
        height: 30,
        marginBottom: 5
    },
    contentPickers: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 5
    },
    stylePicker: {
        flex: 1,
    },
    stylePickerText: {
        fontSize: 8
    },

    countContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    countInput: {
        flex: 1,
        height: 25,
        marginHorizontal: 8,
        paddingHorizontal: 8,
        borderStyle: 'solid',
        borderColor: '#8F7E5A',
        borderWidth: 1
    },
    countButton: {
        width: 25,
        height: 25,
        backgroundColor: '#8F7E5A',
        alignItems: 'center',
        justifyContent: 'center'
    },
    countButtonText: {
        fontSize: 16,
        color: 'white'
    },

    swipeable: {
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EF5252'
    },
    actionIcon: {
        width: 30,
        marginHorizontal: 10
    },
    rightAction: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    }
});

export default CardProduct
