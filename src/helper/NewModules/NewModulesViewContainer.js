import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import NewModules from './NewModulesView.js';

export default compose(
    connect(
        state => ({}),
        dispatch => ({}),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(NewModules);
