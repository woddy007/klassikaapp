import React, {Component} from 'react';
import Navigator from './navigation/Navigator';
import {AsyncStorage} from "react-native";
import * as SecureStore from 'expo-secure-store';
import * as Permissions from 'expo-permissions';
import * as firebase from "firebase";
import {Notifications} from 'expo';
import axios from "axios";
import axiosCustom from "../plugins/axios";

const PUSH_ENDPOINT = 'https://klassika-tc.ru/pushers/recipient/create';

class AppView extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {
        this.registerForPushNotificationsAsync()
        this.initBasket()
        this.initFavourites()
        this.checkUserAuthorization()
    }
    initBasket = async () => {
        const basket = await AsyncStorage.getItem('basket');

        if (basket) {
            let list = JSON.parse(basket)

            for (let key in list) {
                let item = list[key]

                this.props.loadInitStateCarts(item)
            }
        }
    }
    initFavourites = async () => {
        const favorites = await AsyncStorage.getItem('favorites');

        if (favorites) {
            let list = JSON.parse(favorites)

            for (let key in list) {
                let item = list[key]

                this.props.loadInitStateFavourites(item)
            }
        }
    }
    checkUserAuthorization = async () => {
        const token = await AsyncStorage.getItem('token');

        if (token) {
            axiosCustom('get', 'api-shop-shopapp/me').then(response => {
                let user = response.data
                this.props.setUser(user)
            }).catch(error => {
            })
        }
    }

    registerForPushNotificationsAsync = async () => {
        const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);

        if (status !== 'granted') {
            alert('No notification permissions!');
            return;
        }

        let token = await Notifications.getExpoPushTokenAsync();

        let body = new FormData()
        body.append(
            'token', token
        )


        axios({
            method: 'post',
            url: PUSH_ENDPOINT,
            data: body
        })
    }

    render() {
        return (
            <Navigator
                onNavigationStateChange={() => {
                }}
                uriPrefix="/app"
            />
        )
    }
}

export default AppView
