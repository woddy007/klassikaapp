import {compose, lifecycle} from 'recompose';
import {Platform, UIManager} from 'react-native';

import AppView from './AppView';
import {connect} from "react-redux";
import { loadInitState } from "./cart/cart/CartViewState";
import { loadInitStateFavourites } from './favorites/favorites/FavoritesViewState'
import { setUser } from './profile/user_info/UserInfoViewState'

export default compose(
    connect(
        state => ({}),
        dispatch => ({
            loadInitStateCarts: (item) => dispatch(loadInitState(item)),
            loadInitStateFavourites: (product) => dispatch(loadInitStateFavourites(product)),
            setUser: (user) => dispatch(setUser(user))
        }),
    ),
    lifecycle({
        componentWillMount() {
            if (Platform.OS === 'android') {
                UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental') && UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental(true)');
            }
        },
    }),
)(AppView);
