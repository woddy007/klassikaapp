import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    FlatList
} from 'react-native';
import Button from "../../../components/Button";
import CardCart from '../../../components/CardCart'
import common from "../../../styles/common";
import {DropDownHolder} from '../../../components/DropDownAlert';


class Cart extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    getListCart = () => {
        let cart = this.props.cart.list
        let list = []

        for (let key in cart) {
            list.push(cart[key])
        }

        return list
    }
    getCountProduct = () => {
        let cart = this.props.cart.list
        let count = 0
        for (let key in cart) {
            let item = cart[key]
            count += item.count
        }

        return count
    }
    getTotalPrice = () => {
        let cart = this.props.cart.list
        let totalPrice = 0
        for (let key in cart) {
            let item = cart[key]
            totalPrice += item.count * item.product.price
        }

        return totalPrice
    }
    getAccruedBonuses = () => {
        let list = this.props.cart.list
        let bonus = 0

        for( let key in list ){
            let { product, count } = list[key]

            bonus += product.bonus * count
        }

        return bonus
    }

    changeCountProduct = ({product, count}) => {
        this.props.changeCountCardProduct({productInfo: product, newCount: count})
    }
    deleteProduct = ({product, modification}) => {
        let key = product.id + '-' + modification

        this.props.deleteCardProduct(key)

        DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Вы удалили товар из корзины');
    }
    changeModificationProduct = ({product, newModification}) => {
        this.props.changeModificationCardProduct({productInfo: product, newModification})
    }
    toOrderBasket = () => {
        let {delivery, payment} = this.props.cart.orderInformation
        let { user } = this.props.user

        if ( user ){
            if (!delivery.type) {
                this.props.navigation.navigate('CartDelivery')
            } else if (!payment) {
                this.props.navigation.navigate('CartPayments')
            } else {
                this.props.navigation.navigate('CartInformation')
            }
        }else{
            this.props.navigation.navigate('User')
        }
    }

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={styles.page}>
                {
                    (Object.keys(this.props.cart.list).length > 0) ?
                        <ScrollView>
                            <View>
                                <FlatList
                                    data={this.getListCart()}
                                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                                    renderItem={({ item, idx }) => (
                                        <CardCart
                                            key={'cart-product-' + idx}
                                            product={item}
                                            isChangeCount={({product, count}) => this.changeCountProduct({
                                                product,
                                                count
                                            })}
                                            isChangeModification={(product) => this.changeModificationProduct(product)}
                                            isDelete={(product) => this.deleteProduct(product)}
                                        />
                                    )}
                                    keyExtractor={(item, idx) => `cart ${idx}`}
                                />
                            </View>

                            <View style={styles.cartInfo}>
                                <View style={styles.cartInfoLine}>
                                    <Text style={styles.cartInfoText}>Товаров в заказе</Text>
                                    <Text style={styles.cartInfoText}>{this.getCountProduct()} шт.</Text>
                                </View>
                                <View style={styles.cartInfoLine}>
                                    <Text style={styles.cartInfoText}>Бонусов будет начислено</Text>
                                    <Text style={[styles.cartInfoText, { color: '#2F9F00' }]}>{this.getAccruedBonuses()}</Text>
                                </View>
                                <View style={styles.cartInfoLine}>
                                    <Text style={styles.cartInfoTextBold}>Итого</Text>
                                    <Text style={styles.cartInfoTextBold}>{this.getTotalPrice()} ₽</Text>
                                </View>
                                <View style={styles.buttonContent}>
                                    <TouchableOpacity
                                        style={styles.button}
                                        onPress={() => this.toOrderBasket()}
                                    >
                                        <Text style={styles.text}>Перейти к оформлению заказа</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                        :
                        <View style={styles.nullContent}>
                            <Text style={styles.nullContentTitle}>В вашей корзине пусто</Text>
                            <Text style={styles.nullContentCaption}>Вы пока ничего не добавили в Корзину</Text>
                            <View style={styles.nullButtonContent}>
                                <Button
                                    label="НАЧАТЬ ПОКУПКИ"
                                    type="outline"
                                    click={() => {
                                        // navigate('CartDelivery');
                                        navigate('Home');
                                    }}
                                />
                            </View>
                        </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: '#F3F5F7'
    },
    cartInfo: {
        padding: 16,
        paddingBottom: 0
    },
    cartInfoLine: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    cartInfoText: {
        fontSize: 12,
    },
    cartInfoTextBold: {
        fontSize: 16,
        fontWeight: '500'
    },

    button: {
        width: '80%',
        marginHorizontal: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        padding: 11,
        marginTop: 18
    },
    text: {
        fontSize: 13,
        color: 'white',
        fontWeight: '500'
    },
    buttonContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    nullContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nullContentTitle: {
        fontSize: 22,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 12
    },
    nullContentCaption: {
        fontSize: 14,
        fontWeight: '300',
        color: '#7C7C7C',
        marginBottom: 25
    },
    nullButtonContent: {
        width: '60%'
    }
});

export default Cart
