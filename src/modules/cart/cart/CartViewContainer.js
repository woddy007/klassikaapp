import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Cart from './CartView.js';
import {
    deleteCardProduct,
    changeCountCardProduct,
    changeModificationCardProduct,
} from './CartViewState'
import user from "../../profile/user_info/UserInfoViewState";

export default compose(
    connect(
        state => ({
            cart: state.cart,
            user: state.user
        }),
        dispatch => ({
            deleteCardProduct: (product) => dispatch(deleteCardProduct(product)),
            changeCountCardProduct: ({ productInfo, newCount }) => dispatch(changeCountCardProduct({ productInfo, newCount })),
            changeModificationCardProduct: ({ productInfo, newModification }) => dispatch(changeModificationCardProduct({ productInfo, newModification })),
        }),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(Cart);
