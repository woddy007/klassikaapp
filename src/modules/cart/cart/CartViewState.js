import axios from "../../../plugins/axios";
import * as SQLite from 'expo-sqlite';
import {AsyncStorage} from 'react-native';

const ADD_CART_PRODUCT = 'cart/ADD_CART_PRODUCT'
const DELETE_CART_PRODUCT = 'cart/DELETE_CART_PRODUCT'
const CHANGE_COUNT_CART_PRODUCT = 'cart/CHANGE_COUNT_CART_PRODUCT'
const CHANGE_MODIFICATION_CART_PRODUCT = 'cart/CHANGE_MODIFICATION_CART_PRODUCT'
const LOAD_INIT_STATE = 'cart/LOAD_INIT_STATE'
const CHANGE_DELIVERY_INFO = 'cart/CHANGE_DELIVERY_INFO'
const CHANGE_PAYMENT_INFO = 'cart/CHANGE_PAYMENT_INFO'
const CLEAR_CART = 'cart/CLEAR_CART'

const initialState = {
    list: {},
    orderInformation: {
        delivery: {
            type: null,
            city: null,
            street: null,
            house: null,
            apartment: null,
        },
        payment: null,
    }
};

export function loadInitState(item) {
    let list = initialState.list

    if ( !item ){
        return false
    }

    let { product, modification, count } = item
    let key = product.id + '-' + modification

    list[key] = {
        product,
        count,
        modification
    }

    return {
        type: LOAD_INIT_STATE,
        list
    }
}
export async function writeInitState() {
    let basket = initialState.list

    console.log('basket: ', basket)

    try {
        await AsyncStorage.setItem('basket', JSON.stringify(basket));
    } catch (error) {
        // Error saving data
    }
}

export function getTotalPrice() {
    let totalPrice = 0
    let list = initialState.list

    for(let key in list){
        let { count, product } = list[key]

        totalPrice += count * product.price
    }

    return totalPrice
}
export function getCountCart() {
    let list = initialState.list
    let count = 0


    for (let key in list) {
        let item = list[key]
        count += item['count']
    }

    return count
}
export function addCardProduct({product, modification}) {
    let list = initialState.list
    let key = product.id + '-' + modification

    if (!list[key]) {
        list[key] = {
            product,
            count: 1,
            modification: modification
        }
    } else {
        list[key]['count']++
    }

    writeInitState()

    return {
        type: ADD_CART_PRODUCT,
        list
    }
}
export function deleteCardProduct(key) {
    let list = initialState.list
    delete list[key]

    writeInitState()

    return {
        type: DELETE_CART_PRODUCT,
        list
    }
}
export function changeModificationCardProduct({productInfo, newModification}) {
    let {count, modification, product} = productInfo
    let list = initialState.list

    let oldKey = product.id + '-' + modification
    let newKey = product.id + '-' + newModification

    if ( oldKey == newKey ){
        return {
            type: CHANGE_MODIFICATION_CART_PRODUCT,
            list
        }
    }

    if (list[newKey]) {
        list[newKey]['count'] += list[oldKey]['count']
        delete list[oldKey]
    } else {
        let item = list[oldKey]
        item['modification'] = newModification
        list[newKey] = item
        delete list[oldKey]
    }

    writeInitState()

    return {
        type: CHANGE_MODIFICATION_CART_PRODUCT,
        list
    }
}
export function changeCountCardProduct({productInfo, newCount}) {
    let {count, modification, product} = productInfo
    let key = product.id + '-' + modification
    let list = initialState.list

    list[key]['count'] = newCount

    writeInitState()

    return {
        type: CHANGE_COUNT_CART_PRODUCT,
        list
    }
}
export function changeInfoDelivery(info) {
    let orderInformation = initialState.orderInformation

    orderInformation.delivery = info

    return {
        type: CHANGE_DELIVERY_INFO,
        orderInformation
    }
}
export function changePaymentDelivery(type) {
    let orderInformation = initialState.orderInformation

    orderInformation.payment = type

    return {
        type: CHANGE_PAYMENT_INFO,
        orderInformation
    }
}

export function clearCart() {
    let list = initialState.list

    for(let key in list){
        delete list[key]
    }

    writeInitState()

    return {
        type: CLEAR_CART,
        list
    }
}

// Reducer
export default function CartState(state = initialState, action = {}) {
    switch (action.type) {
        case ADD_CART_PRODUCT: {
            let list = action.list

            return {
                ...state,
                list
            }
        }
        case DELETE_CART_PRODUCT: {
            var list = action.list

            return {
                ...state,
                list
            }
        }
        case CHANGE_MODIFICATION_CART_PRODUCT: {
            var list = action.list

            return {
                ...state,
                list
            }
        }
        case CHANGE_COUNT_CART_PRODUCT: {
            let list = action.list

            return {
                ...state,
                list
            }
        }
        case LOAD_INIT_STATE: {
            let list = action.list

            return {
                ...state,
                list
            }
        }
        case CHANGE_DELIVERY_INFO: {
            let orderInformation = action.orderInformation

            return {
                ...state,
                orderInformation
            }
        }
        case CHANGE_PAYMENT_INFO: {
            let orderInformation = action.orderInformation

            return {
                ...state,
                orderInformation
            }
        }
        case CLEAR_CART: {
            let list = action.list

            return {
                ...state,
                list
            }
        }
        default:
            return state;
    }
}
