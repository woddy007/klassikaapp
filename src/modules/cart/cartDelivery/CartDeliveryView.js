import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    Platform,
    Alert,
    Modal,
    BackHandler,
    ActivityIndicator
} from 'react-native';
import {getTotalPrice} from '../cart/CartViewState'
import InputText from '../../../components/InputText'
import Button from "../../../components/Button";
import Form from "../../../components/Form";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";

const iconCheck = require('../../../../assets/icons/check.png');

const RadioButton = (props) => {
    return (
        <View style={[stylesRadio.button, (props.active) ? stylesRadio.buttonActive : '']}>
            {
                (props.active) ?
                    <Image
                        style={stylesRadio.icon}
                        source={iconCheck}
                    />
                    :
                    <View/>
            }
        </View>
    )
}

class CartDelivery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            deliveryList: [],

            deliveryType: null,
            totalPrice: null,
        };
    }

    componentDidMount = () => {
        this.setState({
            totalPrice: getTotalPrice(),
            deliveryType: this.props.cart.orderInformation.delivery.type
        })

        this.loadDeliveryMethods()
    }

    loadDeliveryMethods = () => {
        setTimeout(() => {
            let list = [
                {
                    "id": 8,
                    "name": "Доставка до двери (СДЭК)",
                    "sort": 0,
                    "is_active": 1,
                    "class": "City",
                    "description": null
                },
                {
                    "id": 6,
                    "name": "Самовывоз",
                    "sort": 4,
                    "is_active": 1,
                    "class": "Pickup",
                    "description": null
                }
            ]

            this.setState({
                deliveryList: list
            })
        }, 10)

        // axios('get', 'api-shop-shopapp/getdeliverymethods').then(response => {
        //     let list = response.data
        //
        //     this.setState({
        //         deliveryList: list
        //     })
        // })
    }
    nextPage = () => {
        if (!this.state.deliveryType) {
            DropDownHolder.dropDown.alertWithType('warn', 'Предупреждение', 'Нужно выбрать метод доставки');
        } else {
            if (this.state.deliveryType.id == 8) {
                this.props.navigation.replace({
                    routeName: 'CartDeliveryInfo',
                    params: {
                        type: this.state.deliveryType
                    }
                })
            } else {
                let info = {
                    city: null,
                    street: null,
                    house: null,
                    apartment: null,
                    type: this.state.deliveryType
                }

                this.props.changeInfoDelivery(info)

                this.props.navigation.replace({
                    routeName: 'CartPayments'
                })
            }
        }
    }
    changeDelivery = (item) => {
        this.setState({
            deliveryType: item
        })
    }


    render() {
        return (
            <View style={styles.page}>
                {
                    (this.state.deliveryList.length > 0) ?
                        <ScrollView>
                            {
                                this.state.deliveryList.map((item, idx) => {
                                    return (
                                        <View
                                            style={[styles.section, ( idx == this.state.deliveryList.length - 1)? '' : styles.sectionBorder]}
                                            key={'item-payments-' + idx}
                                        >
                                            <TouchableOpacity
                                                style={styles.sectionItem}
                                                onPress={() => this.changeDelivery(item)}
                                            >
                                                <RadioButton
                                                    active={this.state.deliveryType && this.state.deliveryType.id == item.id}
                                                />
                                                <Text style={styles.sectionItemText}>{ item.name }</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })
                            }

                            <View style={{flexDirection: 'column', flex: 1, alignItems: 'center', marginTop: 15}}>
                                <View style={{marginBottom: 5, width: '80%'}}>
                                    <Button
                                        label="Далее"
                                        click={() => this.nextPage()}
                                        type='primary'
                                    />
                                </View>
                            </View>
                        </ScrollView>
                        :
                        <View style={styles.loadingSection}>
                            <ActivityIndicator size="large" color="#8F7E5A" />
                        </View>
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params

        return {
            headerTitle: 'Доставка',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: '#F3F5F7',
    },
    section: {
        padding: 15,
        backgroundColor: 'white',
    },
    sectionBorder: {
        borderBottomWidth: 1,
        borderColor: '#CDCDCD',
        borderStyle: 'solid'
    },
    sectionTitle: {
        fontSize: 16,
        color: '#000000',
        fontWeight: '500',
        marginBottom: 10
    },
    sectionItem: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    sectionItemText: {
        color: '#6A6A6A',
        flex: 1
    },
    buttonTextNext: {
        color: '#9C9C9C',
    },
    sectionButton: {
        width: '80%',
        marginTop: 15
    },

    button: {
        width: '80%',
        marginHorizontal: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        padding: 11,
        marginTop: 18
    },
    text: {
        fontSize: 13,
        color: 'white',
        fontWeight: '500'
    },
    buttonContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    loadingSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

const stylesRadio = StyleSheet.create({
    button: {
        width: 20,
        height: 20,
        borderRadius: 100,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#CDCDCD',
        marginRight: 13,

        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonActive: {
        borderColor: '#2F9F00',
        backgroundColor: '#2F9F00'
    },
    icon: {
        tintColor: '#FFFFFF',
        width: 15,
        height: 15,
    }
})

export default CartDelivery
