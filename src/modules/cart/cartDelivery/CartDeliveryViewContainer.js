// @flow
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import CartDelivery from './CartDeliveryView';
import { changeInfoDelivery } from '../cart/CartViewState';

export default compose(
  connect(
    state => ({
        cart: state.cart
    }),
    dispatch => ({
        changeInfoDelivery: (info) => dispatch(changeInfoDelivery(info))
    }),
  ),
)(CartDelivery);
