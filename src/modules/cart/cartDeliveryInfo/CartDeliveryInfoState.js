// @flow
type CartDeliveryInfoStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: CartDeliveryInfoStateType = {};

export const ACTION = 'CartDeliveryInfoState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function CartDeliveryInfoStateReducer(state: CartDeliveryInfoStateType = initialState, action: ActionType): CartDeliveryInfoStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
