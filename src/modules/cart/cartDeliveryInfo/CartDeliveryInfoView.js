import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Alert,
    BackHandler, Image
} from 'react-native';
import Form from "../../../components/Form";
import InputText from "../../../components/InputText";
import Button from "../../../components/Button";
import {DropDownHolder} from "../../../components/DropDownAlert";

class CartDeliveryInfo extends Component {
    constructor(props) {
        super(props);

        let {apartment, city, house, street} = this.props.cart.orderInformation.delivery

        this.state = {
            city,
            street,
            house,
            apartment,
        }
    }

    checkForm = () => {
        let bool = true

        if ( !this.state.city ){
            bool = false
        }
        if ( !this.state.street ){
            bool = false
        }
        if ( !this.state.house ){
            bool = false
        }
        if ( !this.state.apartment ){
            bool = false
        }

        return bool
    }
    writeDeliveryInfo = () => {
        if (this.checkForm()) {
            let {city, street, house, apartment} = this.state
            let {type} = this.props.navigation.state.params

            let info = {
                city,
                street,
                house,
                apartment,
                type
            }

            this.props.changeInfoDelivery(info)

            this.props.navigation.replace({
                routeName: 'CartPayments'
            })

        } else {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не все поля заполнены');
        }
    }

    render() {
        return (
            <View style={styles.page}>
                <ScrollView>
                    <View style={styles.modal}>
                        <View style={styles.modalSection}>
                            <InputText
                                label="Населенный пункт"
                                value={this.state.city}
                                rules={val => val && val.length > 0 || 'Заполните поле'}
                                filled
                                onChange={city => {
                                    this.setState({city})
                                }}
                                classContent={{marginBottom: 15}}
                            />
                            <InputText
                                label="Улица"
                                value={this.state.street}
                                rules={val => val && val.length > 0 || 'Заполните поле'}
                                filled
                                onChange={street => {
                                    this.setState({street})
                                }}
                                classContent={{marginBottom: 15}}
                                autoCompleteType='street-address'
                            />
                            <InputText
                                label="Дом"
                                value={this.state.house}
                                rules={val => val && val.length > 0 || 'Заполните поле'}
                                filled
                                onChange={house => {
                                    this.setState({house})
                                }}
                                classContent={{marginBottom: 15}}
                            />
                            <InputText
                                label="Квартира"
                                value={this.state.apartment}
                                rules={val => val && val.length > 0 || 'Заполните поле'}
                                filled
                                onChange={apartment => {
                                    this.setState({apartment})
                                }}
                                classContent={{marginBottom: 15}}
                            />
                        </View>

                        <View style={{flexDirection: 'column', flex: 1}}>
                            <View style={{marginBottom: 5}}>
                                <Button
                                    label="Далее"
                                    click={() => this.writeDeliveryInfo()}
                                    type='primary'
                                />
                            </View>
                            <View>
                                <Button
                                    label="Отмена"
                                    click={() => this.props.navigation.goBack()}
                                    type='outline'
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Оформление заказа',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: '#F3F5F7'
    },
    modal: {
        padding: 14,
        flex: 1,
    },
})

export default CartDeliveryInfo
