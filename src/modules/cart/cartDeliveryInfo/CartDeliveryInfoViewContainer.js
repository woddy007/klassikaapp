// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import CartDeliveryInfoView from './CartDeliveryInfoView';
import { changeInfoDelivery } from '../cart/CartViewState';

export default compose(
  connect(
    state => ({
        cart: state.cart
    }),
    dispatch => ({
        changeInfoDelivery: (info) => dispatch(changeInfoDelivery(info))
    }),
  ),
)(CartDeliveryInfoView);
