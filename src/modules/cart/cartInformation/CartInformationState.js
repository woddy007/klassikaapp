// @flow
type CartInformationStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: CartInformationStateType = {};

export const ACTION = 'CartInformationState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function CartInformationStateReducer(state: CartInformationStateType = initialState, action: ActionType): CartInformationStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
