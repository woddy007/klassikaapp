import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    Platform,
    Modal,
    TextInput
} from 'react-native';
import {getTotalPrice, getCountCart, clearCart} from '../cart/CartViewState'
import InputText from "../../../components/InputText";
import {DropDownHolder} from '../../../components/DropDownAlert'
import axios from "../../../plugins/axios";
import colors from "../../../styles/colors";

const iconCheck = require('../../../../assets/icons/check.png');
const RadioButton = (props) => {
    return (
        <View style={[stylesRadio.button, (props.active) ? stylesRadio.buttonActive : '']}>
            {
                (props.active) ?
                    <Image
                        style={stylesRadio.icon}
                        source={iconCheck}
                    />
                    :
                    <View/>
            }
        </View>
    )
}

class CartInformation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            useBonus: false,
            showModalLoader: false,

            user: null,

            userName: '',
            userPhone: '',
            userComment: ''
        }

        this.refUserName = React.createRef();
        this.refUserPhone = React.createRef();
    }

    componentDidMount = () => {
        this.checkUser()
    }
    componentDidUpdate = (prevProps) => {
        if ( this.state.user != this.props.user.user ){
            this.checkUser()
        }
    }

    checkUser = () => {
        let {user} = this.props.user


        if (user) {
            this.setState({
                user,
                userName: user.name,
                userPhone: user.phone,
            })
        }else{
            this.setState({
                user: null,
                userName: '',
                userPhone: '',
            })
        }
    }

    sendOrder = () => {
        let name = this.state.userName
        let phone = this.state.userPhone

        if (name && phone) {
            this.setState({ showModalLoader: true })

            let order = {}
            let customerForm = this.getUserOrder()
            let products = this.getProductOrder()
            let deliveryForm = this.getDeliveryOrder()
            let paymentFormId = this.props.cart.orderInformation.payment.id


            order['PaymentForm'] = {
                id: paymentFormId
            }
            order['DeliveryForm'] = deliveryForm
            order['CustomerForm'] = {
                name: customerForm.name,
                phone: customerForm.phone,
                email: customerForm.email,
                legal_type: customerForm.legal_type,
                organization_name: customerForm.organization_name,
            }
            order['note'] = this.state.userComment
            order['products'] = products

            axios('post', 'api-shop-shopapp/checkout', order).then(response => {
                console.log('response: ', response)

                DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заказ оформлен');
                this.setState({ showModalLoader: false })
                this.clearCart()
            }).catch(error => {
                console.log('error: ', error.response)
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Ошибка сервера');
                this.setState({ showModalLoader: false })
            })

        } else {
            this.refUserName.current.checkValue(this.state.userName)
            this.refUserPhone.current.checkValue(this.state.userPhone)

            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Заполните контактную информацию');
        }
    }
    getProductOrder = () => {
        let list = this.props.cart.list
        let products = []

        for (let key in list) {
            let {count, modification, product} = list[key]
            modification = this.getModificationIdProduct({product, modification})

            products.push({
                "quantity": count,
                "product_id": product.id,
                "modification": modification
            })
        }

        return products
    }
    getModificationIdProduct = ({product, modification}) => {
        if (modification) {
            let entrySize = modification
            let modificationId = 0


            product.modifications.map(modification => {
                if (modification.variants[0]['id'] == entrySize) {
                    modificationId = modification.id
                }
            })

            return modificationId
        }

        return 0
    }
    getDeliveryOrder = () => {
        let delivery = this.props.cart.orderInformation.delivery
        let address = [delivery.city, delivery.street, delivery.house, delivery.apartment]

        let data = {
            id: delivery.type.id,
        }

        if ( delivery.type.id == 6 ){
            data['shop_id'] = 1
        }

        if ( delivery.type.id == 8 ){
            data['address'] = address.join(' ')
        }

        return data
    }
    getUserOrder = () => {
        let user = {
            name: '',
            phone: '',
            email: '',
            legal_type: '',
            organization_name: '',
        }

        if (!this.state.user) {
            user.name = this.state.userName
            user.phone = this.state.userPhone
        } else {
            user.name = this.state.user.name
            user.phone = this.state.user.phone
            user.email = this.state.user.email
            user.legal_type = this.state.user.legal_type
            user.organization_name = this.state.user.organization_name
        }

        return user
    }
    clearCart = () => {
        const {navigate} = this.props.navigation;
        navigate('Cart')

        this.props.clearCart()
    }

    getAccruedBonuses = () => {
        let list = this.props.cart.list
        let bonus = 0

        for (let key in list) {
            let {product, count} = list[key]

            bonus += product.bonus * count
        }

        return bonus
    }

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={styles.page}>
                <ScrollView style={{flex: 1}}>
                    <View style={styles.section}>
                        <Text style={styles.sectionTitle}>ОБЗОР ВАШЕГО ЗАКАЗА</Text>
                        <View style={styles.sectionContent}>
                            <View style={styles.sectionTextContent}>
                                <Text style={styles.textSection}>{getCountCart()} товар на сумму</Text>
                                <Text style={styles.textSection}>{getTotalPrice()}₽</Text>
                            </View>
                            <View style={styles.sectionTextContent}>
                                <Text style={styles.textSmallSection}>Бонусы</Text>
                                <Text
                                    style={[styles.textSmallSection, {color: '#2F9F00'}]}>{this.getAccruedBonuses()} Б</Text>
                            </View>
                            <View style={styles.sectionTextContent}>
                                <Text style={styles.textBoldSection}>Всего к оплате</Text>
                                <Text style={styles.textBoldSection}>{getTotalPrice()}₽</Text>
                            </View>
                            {
                                (this.props.cart.orderInformation.delivery.type) && <TouchableOpacity
                                    onPress={() => navigate('CartDelivery')}
                                    style={styles.sectionTextContent}
                                >
                                    <Text style={styles.textSection}>Доставка</Text>
                                    <Text
                                        style={styles.textSection}>{this.props.cart.orderInformation.delivery.type.name}</Text>
                                </TouchableOpacity>
                            }
                            {
                                (this.props.cart.orderInformation.payment) && <TouchableOpacity
                                    onPress={() => navigate('CartPayments')}
                                    style={styles.sectionTextContent}
                                >
                                    <Text style={styles.textSection}>Способ оплаты</Text>
                                    <Text
                                        style={styles.textSection}>{this.props.cart.orderInformation.payment.name}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                        <View style={styles.sectionComment}>
                            <TextInput
                                placeholder="Комментарий"
                                onChangeText={userComment => {this.setState({ userComment })}}
                                value={this.state.userComment}
                                multiline
                            />
                        </View>
                    </View>

                    {
                        (!this.state.user)
                            ?
                            <View style={[styles.section, {marginBottom: 0}]}>
                                <Text style={styles.sectionTitle}>Контакты</Text>
                                <View style={styles.sectionContent}>
                                    <InputText
                                        ref={this.refUserName}
                                        label="Ваше имя"
                                        value={this.state.userName}
                                        onChange={userName => {
                                            this.setState({userName})
                                        }}
                                        rules={val => val && val.length > 0 || 'Заполните поле'}
                                        classContent={styles.sectionForm}
                                    />
                                    <InputText
                                        ref={this.refUserPhone}
                                        label="Телефон"
                                        value={this.state.userPhone}
                                        onChange={userPhone => {
                                            this.setState({userPhone})
                                        }}
                                        keyboardType="number-pad"
                                        rules={val => val && val.length > 0 || 'Заполните поле'}
                                        classContent={styles.sectionForm}
                                    />
                                </View>
                            </View>
                            :
                            <View style={styles.section}>
                                {/*<Text style={styles.sectionTitle}>ИСПОЛЬЗОВАТЬ ДЛЯ ОПЛАТЫ</Text>*/}
                                {/*<View style={styles.sectionContent}>*/}
                                {/*    <TouchableOpacity*/}
                                {/*        style={styles.buttonUseBonuses}*/}
                                {/*        onPress={() => this.setState({useBonus: !this.state.useBonus})}*/}
                                {/*    >*/}
                                {/*        <RadioButton*/}
                                {/*            active={this.state.useBonus}*/}
                                {/*        />*/}
                                {/*        <Text style={styles.buttonUseBonusesText}>10 баллов</Text>*/}
                                {/*    </TouchableOpacity>*/}
                                {/*</View>*/}
                            </View>
                    }
                </ScrollView>

                <View style={styles.buttonPayContent}>
                    <TouchableOpacity
                        style={styles.buttonPay}
                        onPress={() => this.sendOrder()}
                    >
                        <Text style={styles.buttonPayText}>ОФОРМИТЬ</Text>
                    </TouchableOpacity>
                </View>



                <Modal
                    visible={this.state.showModalLoader}
                    transparent
                >
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(243, 245, 247, 0.8)'}}>
                        <ActivityIndicator
                            size={'large'}
                            color={colors.primary}
                        />
                    </View>
                </Modal>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Оформление заказа',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: '#F3F5F7',
        paddingBottom: 10
    },
    section: {
        marginBottom: 12
    },
    sectionTitle: {
        padding: 12,
        color: '#8F8F8F',
        fontSize: 12
    },
    sectionContent: {
        backgroundColor: 'white',
        padding: 12,
    },
    sectionTextContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },

    buttonUseBonuses: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonUseBonusesText: {},

    textSection: {
        fontSize: 14,
    },
    textSmallSection: {
        fontSize: 13
    },
    textBoldSection: {
        fontSize: 14,
        fontWeight: 'bold'
    },

    buttonPay: {
        width: '80%',
        marginHorizontal: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        padding: 11,
        marginTop: 18
    },
    buttonPayText: {
        fontSize: 13,
        color: 'white',
        fontWeight: '500'
    },
    buttonPayContent: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    sectionForm: {
        marginBottom: 12
    },

    sectionComment: {
        backgroundColor: 'white',
        marginTop: 5,
        padding: 10
    },
})

const stylesRadio = StyleSheet.create({
    button: {
        width: 20,
        height: 20,
        borderRadius: 100,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#CDCDCD',
        marginRight: 13,

        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonActive: {
        borderColor: '#2F9F00',
        backgroundColor: '#2F9F00'
    },
    icon: {
        tintColor: '#FFFFFF',
        width: 15,
        height: 15,
    }
})

export default CartInformation
