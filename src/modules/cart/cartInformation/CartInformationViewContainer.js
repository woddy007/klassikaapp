// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import CartInformationView from './CartInformationView';
import { clearCart } from '../cart/CartViewState'

export default compose(
  connect(
    state => ({
        cart: state.cart,
        user: state.user
    }),
    dispatch => ({
        clearCart: () => dispatch(clearCart())
    }),
  ),
)(CartInformationView);
