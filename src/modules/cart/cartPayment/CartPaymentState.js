// @flow
type CartPaymentStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: CartPaymentStateType = {};

export const ACTION = 'CartPaymentState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function CartPaymentStateReducer(state: CartPaymentStateType = initialState, action: ActionType): CartPaymentStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
