import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Alert,
    Image,
    ActivityIndicator,
} from 'react-native';
import Button from "../../../components/Button";
import {DropDownHolder} from "../../../components/DropDownAlert";

const iconCheck = require('../../../../assets/icons/check.png');

const RadioButton = (props) => {
    return (
        <View style={[ stylesRadio.button, (props.active)? stylesRadio.buttonActive: '' ]}>
            {
                (props.active) ?
                    <Image
                        style={stylesRadio.icon}
                        source={iconCheck}
                    />
                    :
                    <View />
            }
        </View>
    )
}

class CartPayment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            type: null,

            paymentsList: []
        }
    }

    componentDidMount = () => {
        this.setState({
            type: this.props.cart.orderInformation.payment
        })

        this.loadPaymentsList()
    }

    loadPaymentsList = () => {
        setTimeout(() => {
            let list = [{
                "id": 4,
                "name": "Оплата при получении",
                "sort": 1,
                "is_active": 1,
                "class": "Card",
                "description": null
            }]
            this.setState({
                paymentsList: list
            })
        }, 10)
    }
    nextPage = () => {
        if (!this.state.type) {
            DropDownHolder.dropDown.alertWithType('warn', 'Предупреждение', 'Нужно выбрать способ оплаты');
        } else {
            this.props.changePaymentDelivery(this.state.type)
            this.props.navigation.replace({
                routeName: 'CartInformation'
            })
        }
    }
    changePayments = (item) => {
        this.setState({
            type: item
        })
    }

    render() {
        return (
            <View style={styles.page}>
                {
                    (this.state.paymentsList.length > 0) ?
                        <ScrollView>
                            {
                                this.state.paymentsList.map((item, idx) => {
                                    return (
                                        <View
                                            style={[styles.section, ( idx == this.state.paymentsList.length - 1)? '' : styles.sectionBorder]}
                                            key={'item-payments-' + idx}
                                        >
                                            <TouchableOpacity
                                                style={styles.sectionItem}
                                                onPress={() => this.changePayments(item)}
                                            >
                                                <RadioButton
                                                    active={this.state.type && this.state.type.id == item.id}
                                                />
                                                <Text style={styles.sectionItemText}>{ item.name }</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })
                            }

                            <View style={{flexDirection: 'column', flex: 1, alignItems: 'center', marginTop: 15}}>
                                <View style={{marginBottom: 5, width: '80%'}}>
                                    <Button
                                        label="Далее"
                                        click={() => this.nextPage()}
                                        type='primary'
                                    />
                                </View>
                            </View>

                        </ScrollView>
                        :
                        <View style={styles.loadingSection}>
                            <ActivityIndicator size="large" color="#8F7E5A" />
                        </View>
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params

        return {
            headerTitle: 'Способ оплаты',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: '#F3F5F7'
    },
    section: {
        padding: 15,
        backgroundColor: 'white',
    },
    sectionBorder: {
        borderBottomWidth: 1,
        borderColor: '#CDCDCD',
        borderStyle: 'solid'
    },
    sectionTitle: {
        fontSize: 16,
        color: '#000000',
        fontWeight: '500',
        marginBottom: 10
    },
    sectionItem: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    sectionItemText: {
        color: '#6A6A6A',
        flex: 1
    },
    buttonTextNext: {
        color: '#9C9C9C',
    },

    loadingSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

const stylesRadio = StyleSheet.create({
    button: {
        width: 20,
        height: 20,
        borderRadius: 100,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#CDCDCD',
        marginRight: 13,

        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonActive: {
        borderColor: '#2F9F00',
        backgroundColor: '#2F9F00'
    },
    icon: {
        tintColor: '#FFFFFF',
        width: 15,
        height: 15,
    }
})

export default CartPayment
