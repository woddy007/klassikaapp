// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import CartPaymentView from './CartPaymentView';
import {changePaymentDelivery} from "../cart/CartViewState";

export default compose(
  connect(
    state => ({
        cart: state.cart
    }),
    dispatch => ({
        changePaymentDelivery: (info) => dispatch(changePaymentDelivery(info))
    }),
  ),
)(CartPaymentView);
