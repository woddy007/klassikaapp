import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    SearchBar,
    ActivityIndicator,
    Button,
    Platform,
    SafeAreaView,
} from 'react-native';
import CardCatalog from "../../../components/CardCatalog";
import common from "../../../styles/common";
import CartCategory from "../../../components/CardCategory";
import headers from "../../../styles/headers";
import Loading from "../../../components/Loading";
import Pagination from "../../../components/Pagination";
import axios from "../../../plugins/axios";
import HomeHeaderSearch from "../../../components/HomeHeaderSearch";
import colors from "../../../styles/colors";

const backImage = require('../../../../assets/icons/back.png');
const iconFilter = require('../../../../assets/icons/setting.png');


class Catalog extends Component {
    constructor(props) {
        super(props)

        this.state = {
            categoryId: null,
            page: 1,
            totalPage: 2,

            listProducts: [],
            openLoading: true,

            showFooterLoading: true,
            showTextEmpty: false,
            filter: null
        }
    }

    componentDidMount() {
        const {navigation} = this.props;

        this.setState({
            filter: this.getFilter()
        })

        this.focusListener = navigation.addListener('didFocus', () => {
            if (this.props.products.updateFilter) {
                this.setState({
                    page: 1,
                    listProducts: [],
                    openLoading: true,
                    filter: this.getFilter()
                })

                this.wait(1000).then(() => {
                    this.getList()
                });

                this.props.changeUpdate(false)
            }
        });


        this.getList()
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        curentCategory = this.props.navigation.state.params.categoryId
        prevCategory = prevProps.navigation.state.params.categoryId

        if ( curentCategory != prevCategory ){
            const categoryId = curentCategory

            this.props.navigation.setParams({
                categoryId
            });
        }
    }

    componentWillUnmount = async () => {
        this.props.resetFilterAll()
        this.focusListener.remove()
        this.props.changeUpdate(false)
    }

    wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    getList = () => {
        this.setState({showTextEmpty: false})
        let params = this.getParams()

        axios('get', 'api-shop-shopapp/getproducts?' + params).then(response => {
            let list = this.state.listProducts.concat(response.data)
            let showTextEmpty = false

            this.setState({listProducts: list})
            this.setState({totalPage: response.headers['x-pagination-page-count']})

            if ( list.length <= 0 ){
                showTextEmpty = true
            }

            this.setState({
                openLoading: false,
                showTextEmpty
            })
        }).catch(error => {
            this.setState({
                openLoading: false,
                showTextEmpty: true,
            })
        })
    }
    getParams = () => {
        const navigate = this.props.navigation;
        const categoryId = navigate.state.params.categoryId

        let params = [
            'category_id=' + categoryId,
            'expand=brand',
            'page=' + this.state.page,
            this.state.filter
        ];

        return params.join('&')
    }
    onEndReached = () => {
        this.wait(1000).then(() => {
            if (Number(this.state.totalPage) >= this.state.page + 1) {
                this.setState({
                    page: this.state.page + 1
                })

                this.getList()
            }
        });
    }

    renderFooter = () => {
        return (this.state.page < this.state.totalPage) ?
            <ActivityIndicator
                size="large"
                color={colors.primary}
            /> : <View/>
    }
    renderListEmptyComponent = () => {
        return (this.state.showTextEmpty) ? <View style={styles.blockListEmpty}>
            <Text style={styles.textListEmpty}>Нечего не найдено</Text>
        </View> : <View/>
    }
    addProductFavourites = (product) => {
        this.props.addToFavourites(product)
    }

    getFilter = () => {
        let filterStore = this.props.products.filter
        let filterArray = []

        for (let key in filterStore) {
            let item = filterStore[key]

            item.map(item => {
                filterArray.push(key + '=' + item)
            })
        }

        return filterArray.join('&')
    }

    render() {
        return (
            <SafeAreaView style={{paddingRight: 8, paddingVertical: 10, flex: 1}}>
                <FlatList
                    data={this.state.listProducts}
                    renderItem={({item}) =>
                        <CardCatalog
                            item={item}
                            props={this.props}
                            isAddFavourites={(product) => this.addProductFavourites(product)}
                        />
                    }
                    keyExtractor={(item, idx) => item.external_id + '-' + idx}
                    numColumns={2}
                    onEndReached={() => this.onEndReached()}
                    ListEmptyComponent={() => this.renderListEmptyComponent()}
                    ListFooterComponent={() => this.renderFooter()}
                    removeClippedSubviews={true}
                />
            </SafeAreaView>
        )
    }

    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params

        return {
            headerTitleStyle: {
                justifyContent: (Platform.OS === 'android') ? 'flex-end' : 'center',
            },
            headerRightContainerStyle: {
                padding: 16
            },
            headerTitle: navigation.state.params.categotyName,
            headerRight: () => (
                (params && params.categoryId)
                    ?
                    <TouchableOpacity
                        onPress={() => navigation.navigate('CatalogSort', {
                            title: 'Сортировка',
                            action: null,
                            categoryId: params.categoryId
                        })}
                        style={styles.sortButton}
                    >
                        <Image
                            resizeMode="contain"
                            source={iconFilter}
                            style={styles.iconFilter}
                        />
                    </TouchableOpacity>
                    :
                    <View>
                        <ActivityIndicator
                            size={'small'}
                            color={colors.primary}
                        />
                    </View>
            )
        };
    };
}

const styles = StyleSheet.create({
    flatList: {},
    pageContent: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginLeft: -8
    },
    filtesContent: {
        position: 'absolute',
        width: 100,
        height: 25,
        backgroundColor: 'red',
        alignSelf: 'flex-end',
        alignContent: 'flex-end'
    },

    headerContent: {
        flex: 1,
        textAlign: 'center'
    },
    headerTitle: {
        textAlign: 'center',
        fontSize: 14
    },
    sortButton: {
        padding: 10
    },
    iconFilter: {
        width: 20,
        height: 20
    },

    blockListEmpty: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center'
    },
    textListEmpty: {
        textAlign: 'center',
        fontSize: 21
    },
})

export default Catalog
