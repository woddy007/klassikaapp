import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Catalog from './CatalogView.js';
import {
    addToFavourites,
    chechFavourit
} from '../../favorites/favorites/FavoritesViewState'
import {
    resetFilterAll,
    changeUpdate
} from "./CatalogViewState";


export default compose(
    connect(
        state => ({
            favorites: state.favorites,
            products: state.products
        }),
        dispatch => ({
            addToFavourites: (product) => dispatch(addToFavourites(product)),
            chechFavourit: (id) => chechFavourit(id),
            resetFilterAll: () => dispatch(resetFilterAll()),
            changeUpdate: (bool) => dispatch(changeUpdate(bool)),
        }),
    ),
)(Catalog);
