import axios from "../../../plugins/axios";

const CHANGE_FILTER_ITEM = 'Catalog/CHANGE_FILTER_ITEM'
const CHANGE_FILTER_UPDATE = 'Catalog/CHANGE_FILTER_UPDATE'
const RESET_FILTER = 'Catalog/RESET_FILTER'
const RESET_FILTER_ALL = 'Catalog/RESET_FILTER_ALL'

const initialState = {
    filter: {},
    updateFilter: false
};

export function getAllFilter() {
    let filters = initialState.filter
    let filterAll = []

    for( let key in filters ){
        let filter = filters[key]

        filterAll.push(key + '=' + filter.join(','))
    }

    return filterAll.join('&')
}

export function changeFilterItem( key, list ) {
    let filter = initialState.filter

    filter[key] = list

    return {
        type: CHANGE_FILTER_ITEM,
        params: {filter, updateFilter: true}
    }
}
export function resetFilter(key) {
    let filter = initialState.filter

    if ( filter[key] ){
        delete filter[key]
    }

    return {
        type: RESET_FILTER,
        params: {filter, updateFilter: true}
    }
}
export function resetFilterAll() {
    let filter = initialState.filter

    filter = {}

    return {
        type: RESET_FILTER_ALL,
        params: {filter, updateFilter: true}
    }
}
export function changeUpdate(bool) {
    let updateFilter = initialState.updateFilter

    updateFilter = bool

    return {
        type: CHANGE_FILTER_UPDATE,
        updateFilter
    }
}


// Reducer
export default function CatalogState(state = initialState, action) {
    switch (action.type) {
        case CHANGE_FILTER_ITEM:{
            let filter = action.params.filter
            let updateFilter = action.params.updateFilter

            return Object.assign({}, state, {
                filter,
                updateFilter
            });
        }
        case CHANGE_FILTER_UPDATE:{
            let updateFilter = action.updateFilter
            return {
                ...state,
                updateFilter
            };
        }
        case RESET_FILTER:{
            let filter = action.params.filter
            let updateFilter = action.params.updateFilter

            return Object.assign({}, state, {
                filter,
                updateFilter
            });
        }
        case RESET_FILTER_ALL:{
            let filter = action.params.filter
            let updateFilter = action.params.updateFilter

            return Object.assign({}, state, {
                filter,
                updateFilter
            });
        }
        default:
            return state;
    }
}
