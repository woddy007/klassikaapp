import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image, Platform, ActivityIndicator,
} from 'react-native';
import Button from "../../../components/Button";
import common from "../../../styles/common";
import colors from "../../../styles/colors";
import {DropDownHolder} from "../../../components/DropDownAlert";
import axios from "../../../plugins/axios";

const iconNext = require('../../../../assets/icons/next-right.png');

class CatalogSort extends Component {
    constructor(props) {
        super(props)

        this.state = {
            list: [],
            loadingList: true
        }

        this.backHandler = null;
    }

    componentDidMount = () => {
        this.props.navigation.setParams({handleFilterReset: this.resetFilter.bind(this)});

        this.loadListFilter()
    }

    toCatalog = () => {
        const {navigate} = this.props.navigation;
        navigate('Catalog')
    }

    loadListFilter = () => {
        let categoryId = this.props.navigation.state.params.categoryId

        axios('get', 'api-shop-shopapp/filter-data?id=' + categoryId + '&options[categories][active]=1').then(response => {
            console.log('response: ', response)

            this.setState({
                list: response.data.options,
                loadingList: false
            })
        })
    }

    resetFilter = () => {
        DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Все фильтры сброшены');

        this.props.resetFilterAll()
    }
    clickSort = (item) => {
        const navigation = this.props.navigation

        navigation.navigate('CatalogSortItem', {
            title: item.name,
            action: item.filterName,
            list: item.variants
        })
    }
    clickCard = () => {
        this.setState({countProduct: this.state.countProduct + 10})
    }
    getCountFilterActiveItem = (item) => {
        let { filter } = this.props.filter

        if ( filter == '{}' ){
            return 0
        }

        for( let key in filter ){
            let itemFilter = filter[key]

            if ( item.filterName == key ){
                return itemFilter.length
            }
        }

        return 0
    }

    render() {
        const navigation = this.props.navigation

        return (
            <View style={styles.page}>

                {
                    (this.state.loadingList)
                        ?
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <ActivityIndicator
                                size={'large'}
                                color={colors.primary}
                            />
                        </View>
                        :
                        <ScrollView>
                            {
                                this.state.list.map((item, idx) => {
                                    return (
                                        <TouchableOpacity
                                            style={[common.blockFilter, (idx == this.state.list.length - 1) ? common.blockFilterLast : '']}
                                            key={idx}
                                            onPress={() => this.clickSort(item)}
                                        >
                                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                                <Text style={common.blockFilterText}>{item.name}</Text>
                                                {
                                                    (this.getCountFilterActiveItem(item))
                                                        ?
                                                        <View style={styles.countFilterItemView}>
                                                            <Text style={styles.countFilterItemText}>{this.getCountFilterActiveItem(item)}</Text>
                                                        </View>
                                                        :
                                                        <View/>
                                                }
                                            </View>
                                            <Image
                                                resizeMode="contain"
                                                source={iconNext}
                                                style={common.blockFilterIcon}
                                            />
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                }

                <View style={{paddingHorizontal: 14, marginTop: 14}}>
                    <Button
                        label={"Применить"}
                        click={() => {
                            this.toCatalog()
                        }}
                        type="primary"
                    />
                </View>
            </View>
        )
    }

    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params

        return {
            headerRightContainerStyle: {
                padding: 16
            },
            headerTitle: 'Фильтр',
            headerRight: () => (
                (!params.handleFilterReset)
                    ?
                    <View>
                        <ActivityIndicator
                            size={'small'}
                            color={colors.primary}
                        />
                    </View>
                    :
                    <TouchableOpacity
                        style={styles.buttonResetFilter}
                        onPress={() => params.handleFilterReset()}
                    >
                        <Text style={styles.textResetFilter}>Сбросить</Text>
                    </TouchableOpacity>
            )
        };
    };
}

const styles = StyleSheet.create({
    page: {
        backgroundColor: '#F3F5F7',
        paddingVertical: 14,
        flex: 1
    },
    block: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 16,
    },
    input: {
        height: 35,
        flex: 1,
        backgroundColor: '#F3F5F7',
        paddingHorizontal: 15,
    },
    searchButton: {
        position: 'absolute',
        right: 31,
        top: '50%',
        transform: [
            {translateY: -17}
        ]
    },
    searchIcon: {
        width: 15,
        height: 15,
        tintColor: '#959595',
    },

    buttonResetFilter: {
        borderWidth: 0.5,
        borderStyle: 'solid',
        borderColor: '#8F7E5A',
        borderRadius: 3,
        paddingHorizontal: 10,
        paddingVertical: 3,
        height: 26
    },
    textResetFilter: {
        color: '#8F7E5A'
    },
    countFilterItemView: {
        width: 16,
        height: 16,
        borderRadius: 100,
        backgroundColor: '#2F9F00',
        marginLeft: 5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    countFilterItemText: {
        color: 'white',
        fontSize: 12
    }
})

export default CatalogSort
