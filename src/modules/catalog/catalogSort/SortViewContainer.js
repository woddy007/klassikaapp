import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Sort from './SortView';
import products, {resetFilterAll} from "../catalog/CatalogViewState";


export default compose(
    connect(
        state => ({
            filter: state.products
        }),
        dispatch => ({
            resetFilterAll: () => dispatch(resetFilterAll())
        }),
    ),
)(Sort);
