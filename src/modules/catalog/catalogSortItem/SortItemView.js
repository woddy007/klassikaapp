import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image, ActivityIndicator,
} from 'react-native';
import Button from "../../../components/Button";
import common from "../../../styles/common";
import colors from "../../../styles/colors";

const iconCheck = require('../../../../assets/icons/check.png');

const RadioButton = (props) => {
    return (
        <View style={[stylesRadio.button, (props.active) ? stylesRadio.buttonActive : '']}>
            {
                (props.active) ?
                    <Image
                        style={stylesRadio.icon}
                        source={iconCheck}
                    />
                    :
                    <View/>
            }
        </View>
    )
}

class CatalogSortItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            entry: [],
        }

        this.backHandler = null;
    }

    componentDidMount = () => {
        this.props.navigation.setParams({handleFilterReset: this.resetFilter.bind(this)});

        let action = this.props.navigation.state.params.action
        let listState = this.props.products.filter[action]

        if ( listState ){
            this.setState({
                entry: listState
            })
        }
    }

    resetFilter = () => {
        let action = this.props.navigation.state.params.action

        this.setState({ entry: [] })

        this.props.resetFilter(action)
    }
    clickFilter = (item) => {
        let params = this.props.navigation.state.params

        let action = params.action
        let list = params.list
        let entry = this.state.entry

        if ( entry.indexOf(item.value) == -1 ){
            entry.push(item.value)
        }else{
            entry.splice(entry.indexOf(item.value), 1)
        }

        this.setState({ entry: entry })

        this.props.changeFilterItem(action, entry)
    }
    isActive = (itemSize) => {
        if (this.state.entry.indexOf(itemSize.value) != -1){
            return true
        }

        return false
    }

    toCatalog = () => {
        const {navigate} = this.props.navigation;
        navigate('Catalog')
    }

    render() {
        const navigation = this.props.navigation
        const list = navigation.state.params.list

        return (
            <View style={styles.page}>
                <ScrollView>
                    {
                        list.map((item, idx) => {
                            return (
                                <TouchableOpacity
                                    key={idx}
                                    style={[common.blockFilter, ( idx == list.length - 1 )? common.blockFilterLast: '']}
                                    onPress={() => this.clickFilter(item)}
                                >
                                    <Text style={common.blockFilterText}>{item.label}</Text>
                                    <RadioButton
                                        active={this.isActive(item)}
                                    />
                                </TouchableOpacity>
                            )
                        })
                    }
                </ScrollView>

                <View style={{paddingHorizontal: 14, marginTop: 14}}>
                    <Button
                        label={'Применить'}
                        type="primary"
                        click={() => this.toCatalog()}
                    />
                </View>
            </View>
        )
    }

    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params

        return {
            headerTitleStyle: {},
            headerRightContainerStyle: {
                padding: 16
            },
            headerTitle: navigation.state.params.title,
            headerRight: () => (
                (!params.handleFilterReset)
                    ?
                    <View>
                        <ActivityIndicator
                            size={'small'}
                            color={colors.primary}
                        />
                    </View>
                    :
                    <TouchableOpacity
                        style={styles.buttonResetFilter}
                        onPress={() => params.handleFilterReset()}
                    >
                        <Text style={styles.textResetFilter}>Сбросить</Text>
                    </TouchableOpacity>
            )
        };
    };
}

const styles = StyleSheet.create({
    page: {
        backgroundColor: '#F3F5F7',
        paddingVertical: 14,
        flex: 1
    },
    block: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 16,
    },
    input: {
        height: 35,
        flex: 1,
        backgroundColor: '#F3F5F7',
        paddingHorizontal: 15,
    },
    searchButton: {
        position: 'absolute',
        right: 31,
        top: '50%',
        transform: [
            {translateY: -17}
        ]
    },
    searchIcon: {
        width: 15,
        height: 15,
        tintColor: '#959595',
    },

    buttonResetFilter: {
        borderWidth: 0.5,
        borderStyle: 'solid',
        borderColor: '#8F7E5A',
        borderRadius: 3,
        paddingHorizontal: 10,
        paddingVertical: 3,
        height: 26
    },
    textResetFilter: {
        color: '#8F7E5A'
    },

    blockCheck: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 20,
        height: 20,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#CDCDCD'
    },
    blockCheckChecked: {
        borderColor: '#236B9E',
        backgroundColor: '#236B9E'
    },
    blockCheckIcon: {
        width: 15,
        height: 15,
        tintColor: 'white'
    },
})
const stylesRadio = StyleSheet.create({
    button: {
        width: 20,
        height: 20,
        borderRadius: 100,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#CDCDCD',
        marginRight: 13,

        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonActive: {
        borderColor: '#2F9F00',
        backgroundColor: '#2F9F00'
    },
    icon: {
        tintColor: '#FFFFFF',
        width: 15,
        height: 15,
    }
})

export default CatalogSortItem
