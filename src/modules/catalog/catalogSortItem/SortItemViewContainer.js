import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import SortItem from './SortItemView';
import { changeFilterItem, resetFilter } from '../catalog/CatalogViewState'

export default compose(
    connect(
        state => ({
            products: state.products
        }),
        dispatch => ({
            changeFilterItem: (filterName, list) => dispatch(changeFilterItem(filterName, list)),
            resetFilter: (key) => dispatch(resetFilter(key))
        }),
    ),
)(SortItem);
