export default {
    list: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginLeft: -8,
        marginBottom: -8
    },
}
