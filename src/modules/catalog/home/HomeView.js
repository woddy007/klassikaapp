import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image, StatusBar
} from 'react-native';
import CartCategory from '../../../components/CardCategory'
import common from '../../../styles/common'
import style from './HomeStyle'
import Loading from "../../../components/Loading";
import headers from "../../../styles/headers";
import HomeHeaderSearch from "../../../components/HomeHeaderSearch";

const cat3 = require('../../../../assets/category/3.png');
const cat35 = require('../../../../assets/category/35.png');
const cat44 = require('../../../../assets/category/44.png');
const cat50 = require('../../../../assets/category/50.png');
const cat66 = require('../../../../assets/category/66.png');
const cat102 = require('../../../../assets/category/102.png');
const cat55 = require('../../../../assets/category/55.png');
const cat159 = require('../../../../assets/category/159.png');
const backImage = require('../../../../assets/icons/back.png');

class Home extends Component {
    getCategoriesList = () => {
        let list = [
            {
                name: 'Трикотаж',
                image: cat102,
                id: 102
            },
            {
                name: 'Брюки',
                image: cat35,
                id: 35
            },
            {
                name: 'Рубашки',
                image: cat66,
                id: 66
            },
            {
                name: 'Верхняя Одежда',
                image: cat44,
                id: 44
            },
            {
                name: 'Аксессуары',
                image: cat3,
                id: 3
            },
            {
                name: 'Костюм',
                image: cat50,
                id: 50
            },
            {
                name: 'Пиджак',
                image: cat55,
                id: 55
            },
            {
                name: 'Ликвидация',
                image: cat159,
                id: 159
            },
        ]

        return list
    }

    render() {
        return (
            <View style={{ flex: 1, paddingVertical: 16, backgroundColor: '#F3F5F7', }}>
                <ScrollView style={{ flexGrow: 1, paddingHorizontal: 16, }}>
                    <View
                        style={style.list}
                    >
                        {
                            this.getCategoriesList().map((item, idx) => {
                                return <CartCategory
                                    key={idx}
                                    item={item}
                                    width={((idx + 1) % 3 == 0) ? '100%' : '50%'}
                                    props={this.props}
                                />
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: "Классика",
            // headerTitle: () => <HomeHeaderSearch />,
        };
    };
}

export default Home
