import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image, Alert, Platform
} from 'react-native';
import common from "../../../styles/common";
import headers from "../../../styles/headers";
import SliderImage from "../../../components/SliderImage";
import axios from "../../../plugins/axios";
import Loading from "../../../components/Loading";
import {chechFavourit} from '../../favorites/favorites/FavoritesViewState'
import {DropDownHolder} from "../../../components/DropDownAlert";

const wishIcon = require('../../../../assets/icons/heart.png');
const wishFullIcon = require('../../../../assets/icons/heartFull.png');
const bonusesIcon = require('../../../../assets/icons/tag.png');

class Product extends Component {
    constructor() {
        super();

        this.state = {
            productId: null,
            product: null,
            entrySize: null
        }
    }

    componentDidMount = () => {
        const navigate = this.props.navigation;
        const state = navigate.state

        this.getInfoProduct(state.params.productId)
    }
    getInfoProduct = (productId) => {
        axios('get', 'api-shop-shopapp/product?id=' + productId).then(response => {
            console.log(response.data)
            this.setState({product: response.data})
        }).catch(error => {

        })
    }
    getCharacteristics = () => {
        let list = []

        let characteristics = this.state.product.characteristics

        for (let key in characteristics) {
            list.push({
                name: characteristics[key]['name'],
                value: characteristics[key]['values'][0],
            })
        }

        return list
    }
    buttonWish = () => {
        this.props.addToFavourites(this.state.product)
    }
    entrySize = (item) => {
        this.setState({
            entrySize: item.id
        })
    }
    addToCart = () => {
        if (!this.state.entrySize && this.state.product.variants.length > 0) {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не выбран размер');
        } else {
            let data = {
                product: this.state.product,
                modification: this.state.entrySize
            }

            this.setState({
                entrySize: null
            })

            this.props.addCardProduct(data)

            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Вы добавили товар в корзину.');
        }
    }


    render() {
        let item = this.state.product
        return (
            <View style={{paddingVertical: 16}}>
                <ScrollView style={{paddingHorizontal: 16}}>
                    {
                        (this.state.product) ?
                            <View>
                                <View style={styles.imageContent}>
                                    <SliderImage photos={item.photos}/>
                                </View>
                                <View style={styles.info}>
                                    {(item.brand) && <Text style={styles.infoBrand}>{item.brand.name}</Text>}
                                    <Text style={styles.infoName}>{item.name}</Text>
                                    <Text style={styles.infoArticle}>Артикул: {item.code}</Text>
                                    <View style={styles.prices}>
                                        <Text style={styles.priceNew}>{item.price} ₽</Text>
                                        {
                                            (item.price_old) ?
                                                <Text style={styles.priceOld}>{item.price_old} ₽
                                                    (-{item.sale}%)</Text> :
                                                <View></View>
                                        }
                                    </View>
                                    <View style={styles.miniInfo}>
                                        <TouchableOpacity
                                            style={styles.wishBlock}
                                            onPress={() => this.buttonWish()}
                                        >
                                            <View style={styles.wishImageContent}>
                                                <Image
                                                    style={{width: 15, height: 15}}
                                                    source={(chechFavourit(item.id)) ? wishFullIcon : wishIcon}
                                                />
                                            </View>
                                            <Text style={styles.wishText}>Добавить в избранное</Text>
                                        </TouchableOpacity>

                                        <View style={styles.bonusesBlock}>
                                            <View style={styles.bonusesImageContent}>
                                                <Image
                                                    style={{width: 15, height: 15, tintColor: '#959595'}}
                                                    source={bonusesIcon}
                                                />
                                            </View>
                                            <Text style={styles.bonusesText}>{item.bonus} Бонусов будет начислено</Text>
                                        </View>
                                    </View>
                                    <View style={styles.separation}></View>
                                    <View>
                                        {
                                            item.variants.map((item, idx) => {
                                                return (
                                                    <View
                                                        style={styles.actions}
                                                        key={'item-sizes-' + idx}
                                                    >
                                                        <View style={styles.actionsTopLine}>
                                                            <Text style={styles.sizeTitle}>{item.name}</Text>
                                                        </View>

                                                        <View style={styles.blockSizes}>
                                                            {
                                                                item.variants.map((size, idx) => {
                                                                    return (
                                                                        <TouchableOpacity
                                                                            style={[styles.buttonSize, (this.state.entrySize == size.id) ? styles.buttonSizeActive : '']}
                                                                            key={'item-size-' + idx}
                                                                            onPress={() => this.entrySize(size)}
                                                                        >
                                                                            <Text
                                                                                style={[styles.bonusesText, (this.state.entrySize == size.id) ? styles.bonusesTextActive : '']}
                                                                            >
                                                                                {size.label}
                                                                            </Text>
                                                                        </TouchableOpacity>
                                                                    )
                                                                })
                                                            }
                                                        </View>
                                                    </View>
                                                )
                                            })
                                        }
                                        <TouchableOpacity
                                            style={styles.buttonToCart}
                                            onPress={() => this.addToCart()}
                                        >
                                            <Text style={styles.buttonToCartText}>В КОРЗИНУ</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.separation}></View>
                                    <Text style={styles.blockTitle}>Описание товара</Text>
                                    <View style={styles.descriptionProduct}>
                                        {
                                            this.getCharacteristics().map((item, idx) => {
                                                return (
                                                    <View
                                                        key={idx}
                                                        style={styles.descriptionProductLine}
                                                    >
                                                        <Text style={styles.descriptionProductName}>{item.name}</Text>
                                                        <Text style={styles.descriptionProductValue}>{item.value}</Text>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>

                                </View>
                            </View>
                            :
                            <Loading open={!this.state.product}/>
                    }
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    separation: {
        width: '100%',
        height: 1,
        backgroundColor: '#CDCDCD',
        marginBottom: 11
    },

    imageContent: {
        marginBottom: 18
    },
    image: {},
    info: {},
    infoBrand: {
        color: '#989898',
        fontSize: 16,
        fontWeight: '300',
        marginBottom: 7
    },
    infoName: {
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 3
    },
    infoArticle: {
        fontSize: 12,
        marginBottom: 7
    },

    prices: {
        flexDirection: 'row',
        marginBottom: 17,
        alignItems: 'flex-end'
    },
    priceNew: {
        fontSize: 16,
        color: '#EF5252',
        marginRight: 7,
        fontWeight: '500'
    },
    priceOld: {
        fontSize: 12,
        color: '#959595'
    },

    miniInfo: {
        flex: 1,
        flexWrap: 'wrap',
        marginBottom: 16
    },

    wishBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 6
    },
    wishImageContent: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5
    },
    wishText: {
        fontSize: 14
    },

    bonusesBlock: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bonusesImageContent: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5
    },
    bonusesText: {
        color: '#959595',
        fontSize: 14
    },

    actions: {
        marginBottom: 17
    },
    actionsTopLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 9
    },
    sizeTitle: {
        fontSize: 12,
    },
    sizeTable: {
        fontSize: 12,
        color: '#EF5252'
    },
    blockSizes: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginRight: -6,
        marginBottom: 12
    },
    buttonSize: {
        minWidth: 29,
        height: 29,
        borderColor: '#CDCDCD',
        borderWidth: 1,
        borderStyle: 'solid',
        marginRight: 6,
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 7
    },
    buttonSizeActive: {
        borderColor: 'block',
    },
    buttonSizeActive: {
        borderColor: '#000000'
    },
    buttonSizeText: {
        fontSize: 12,
        fontWeight: '500',
        color: '#000000'
    },
    bonusesTextActive: {
        fontSize: 12,
        fontWeight: '500',
        color: 'black'
    },

    descriptionProduct: {},
    descriptionProductLine: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 9
    },
    descriptionProductName: {
        color: '#989898',
        textTransform: 'uppercase',
        fontSize: 10,
        fontWeight: 'bold',
        width: '40%'
    },
    descriptionProductValue: {
        fontSize: 12,
    },

    blockTitle: {
        fontSize: 14,
        marginBottom: 12,
        fontWeight: '500'
    },

    buttonToCart: {
        width: '100%',
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8F7E5A'
    },
    buttonToCartText: {
        fontSize: 12,
        fontWeight: '500',
        color: 'white'
    },
});

export default Product
