import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Product from './ProductView.js';
import { addCardProduct } from '../../cart/cart/CartViewState'
import favorites, { addToFavourites } from '../../favorites/favorites/FavoritesViewState'

export default compose(
    connect(
        state => ({
            cart: state.cart
        }),
        dispatch => ({
            addCardProduct: ({product, modification}) => dispatch(addCardProduct({product, modification})),
            addToFavourites: (product) => dispatch(addToFavourites(product))
        }),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(Product);
