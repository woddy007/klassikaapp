import axios from "../../../plugins/axios";

const initialState = {
    product: null
};

// Reducer
export default function ProductState(state = initialState, action = {}) {
    switch (action.type) {
        default:
            return state;
    }
}
