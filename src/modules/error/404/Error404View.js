import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image
} from 'react-native';
import Button from "../../../components/Button";
import axios from "../../../plugins/axios";

const logo = require('../../../../assets/logo.png')

class Error404 extends Component {
    constructor(props){
        super(props)

        this.state = {
            isLoading: false
        }
    }

    update = () => {
        this.setState({ isLoading: true })

        axios('get', '/admin/api-shop-shopapp/checkconnect').then(response => {
            this.setState({ isLoading: false })
            this.props.navigation.goBack()
        }).catch(error => {
            this.setState({ isLoading: false })
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <Image
                    resizeMode="contain"
                    style={styles.image}
                    source={logo}
                />
                <Text style={styles.message}>Не удалось получить ответ от сервера</Text>

                <View style={{ width: '70%' }}>
                    <Button
                        label="Обновить"
                        type="primary"
                        click={() => this.update()}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        backgroundColor: '#F3F5F7',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        maxWidth: '60%',
        marginBottom: 18
    },
    message: {
        marginBottom: 15
    },
    button: {

    },
    buttonText: {

    }
});

export default Error404
