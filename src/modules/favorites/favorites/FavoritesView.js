import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
    Text,
    TouchableOpacity, Alert
} from 'react-native';
import common from '../../../styles/common';
import CardFavorites from '../../../components/CardFavorites'
import EmptyPage from "../../../components/EmptyPage";
import Button from "../../../components/Button";
import CardCart from "../../../components/CardCart";

class Favorites extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    getList = () => {
        let favorites = this.props.favorites.list
        let list = []

        for ( let key in favorites ){
            list.push( favorites[key] )
        }

        return list
    }
    productToCart = ({ product, modification }) => {
        this.props.addCardProduct({ product, modification })
        this.props.removeFavourites(product.id)
    }
    deleteProduct = (product) => {
        this.props.removeFavourites(product.id)
    }

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={styles.page}>
                {
                    (Object.keys(this.props.favorites.list).length > 0)?
                        <ScrollView
                            style={styles.page}
                        >
                            {
                                this.getList().map((item, idx) => {
                                    return (
                                        <CardFavorites
                                            key={'cart-favorit-' + idx}
                                            product={ item }
                                            isToCart={({ product, modification }) => this.productToCart({ product, modification })}
                                            isDelete={( product ) => this.deleteProduct( product )}
                                        />
                                    )
                                })
                            }
                        </ScrollView>
                        :
                        <View style={styles.nullContent}>
                            <Text style={styles.nullContentTitle}>Здесь пока пусто</Text>
                            <Text style={styles.nullContentCaption}>Вы пока ничего не добавили в Избранное</Text>
                            <View style={styles.nullButtonContent}>
                                <Button
                                    label="СМОТРЕТЬ ТОВАРЫ"
                                    type="outline"
                                    click={() => {
                                        navigate('Home');
                                    }}
                                />
                            </View>
                        </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        paddingVertical: 14,
        backgroundColor: '#F3F5F7',
        flex: 1
    },

    nullContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nullContentTitle: {
        fontSize: 22,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 12
    },
    nullContentCaption: {
        fontSize: 14,
        fontWeight: '300',
        color: '#7C7C7C',
        marginBottom: 25
    },
    nullButtonContent: {
        width: '60%'
    }
});

export default Favorites
