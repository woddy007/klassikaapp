import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Favorites from './FavoritesView.js';
import { addCardProduct } from '../../cart/cart/CartViewState';
import { removeFavourites } from './FavoritesViewState';


export default compose(
    connect(
        state => ({
            favorites: state.favorites
        }),
        dispatch => ({
            addCardProduct: ({product, modification}) => dispatch(addCardProduct({product, modification})),
            removeFavourites: (id) => dispatch(removeFavourites(id))
        }),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(Favorites);
