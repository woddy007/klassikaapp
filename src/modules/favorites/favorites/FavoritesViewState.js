import axios from "../../../plugins/axios";
import {AsyncStorage} from "react-native";

const ADD_FAVOURITES_PRODUCT = 'favorites/ADD_FAVOURITES_PRODUCT'
const REMOVE_FAVOURITES_PRODUCT = 'favorites/REMOVE_FAVOURITES_PRODUCT'
const LOAD_INIT_STATE = 'favorites/LOAD_INIT_STATE'

const initialState = {
    list: {}
};

export function loadInitStateFavourites(product) {
    let list = initialState.list

    if ( !product ){
        return false
    }

    list[product.id] = product

    return {
        type: LOAD_INIT_STATE,
        list
    }
}
export async function writeInitState() {
    let favorites = initialState.list

    try {
        await AsyncStorage.setItem('favorites', JSON.stringify(favorites));
    } catch (error) {
        // Error saving data
    }
}

export function chechFavourit(id) {
    let list = initialState.list

    if ( list[id] ){
        return true
    }

    return false
}
export function addToFavourites(product) {
    let list = initialState.list

    if ( !list[product.id] ){
        list[product.id] = product
    }else{
        delete list[product.id]
    }

    writeInitState()

    return {
        type: ADD_FAVOURITES_PRODUCT,
        list
    }
}
export function getCountFavourites() {
    return Object.keys(initialState.list).length
}
export function removeFavourites(id) {
    let list = initialState.list

    delete list[id]

    writeInitState()

    return {
        type: REMOVE_FAVOURITES_PRODUCT,
        list
    }
}

// Reducer
export default function FavoritesState(state = initialState, action = {}) {
    switch (action.type) {
        case ADD_FAVOURITES_PRODUCT: {
            var list = action.list

            return {
                ...state,
                list
            }
        }
        case REMOVE_FAVOURITES_PRODUCT: {
            var list = action.list

            return {
                ...state,
                list
            }
        }
        case LOAD_INIT_STATE: {
            var list = action.list

            return {
                ...state,
                list
            }
        }
        default:
            return state;
    }
}
