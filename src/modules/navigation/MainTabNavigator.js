import React from 'react';
import {
    Image,
    View,
    StyleSheet,
    Text,
    StatusBar
} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {getCountCart} from '../cart/cart/CartViewState'

//pages

//каталог
import HomeScreen from '../catalog/home/HomeViewContainer';
import CatalogScreen from '../catalog/catalog/CatalogViewContainer';
import CatalogSortScreen from '../catalog/catalogSort/SortViewContainer';
import CatalogSortItemScreen from '../catalog/catalogSortItem/SortItemViewContainer';
import HomeHeaderSearch from '../../components/HomeHeaderSearch';
import ProductScreen from '../catalog/product/ProductViewContainer';

//корзина
import CartScreen from '../cart/cart/CartViewContainer';
import CartDeliveryScreen from '../cart/cartDelivery/CartDeliveryViewContainer'
import CartPaymentsScreen from '../cart/cartPayment/CartPaymentViewContainer'
import CartDeliveryInfoScreen from '../cart/cartDeliveryInfo/CartDeliveryInfoViewContainer'
import CartInformationScreen from '../cart/cartInformation/CartInformationViewContainer'

//Избранное
import FavoritesScreen from '../favorites/favorites/FavoritesViewContainer';


// профиль
import AuthLoadingScreen from "../profile/auth_loading/AuthLoadingViewContainer";
import AuthorizationScreen from "../profile/authorization/AuthorizationContainer";
import ConfirmRegistrationScreen from "../profile/confirm_registration/ConfirmRegistrationViewContainer";
import ProfileScreen from "../profile/profile/ProfileViewContainer";
import UserInfoScreen from "../profile/user_info/UserInfoViewContainer";
import BonusAccountScreen from "../profile/bonus_account/BonusAccountViewContainer";
import ChangePasswordScreen from "../profile/change_password/ChangePasswordViewContainer";
import RegestrationScreen from "../profile/regestration/RegestrationViewContainer";
import VirtualCardScreen from "../profile/virtual_card/VirtualCardViewContainer";
import HistoryScreen from "../profile/history/HistoryViewContainer";

//end pages

//icons
const iconHome = require('../../../assets/images/tabbar/menu.png');
const iconFavorites = require('../../../assets/images/tabbar/heart.png');
const iconCart = require('../../../assets/images/tabbar/shopping-cart.png');
const iconUser = require('../../../assets/images/tabbar/user.png');

const styles = StyleSheet.create({
    tabBarItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 5,
        color: '#7C7C7C',
    },
    tabBarIcon: {
        width: 15,
        height: 15,
    },
    tabBarIconFocused: {
        tintColor: '#EF5252',
    },
    tabBarTitle: {
        color: '#7C7C7C',
        fontWeight: '300',
        marginTop: 5,
        fontSize: 10,

    },

    badge: {
        position: 'absolute',
        right: 0,
        top: -10,
        width: 20,
        height: 20,
        backgroundColor: '#8F7E5A',
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    badgeText: {
        color: 'white',
        fontSize: 10
    }
});

const TransitionConfig = () => ({
    screenInterpolator: ({layout, position, scene}) => {
        let pages = [
            'CatalogSort',
            'CatalogSortItem',
            'UserInfo',
            'BonusAccount',
            'ChangePassword',
            'VirtualCard',
            'History'
        ]
        let routeName = scene.route.routeName

        if (pages.indexOf(routeName) >= 0) {
            const {index} = scene
            const {initWidth} = layout

            const translateX = position.interpolate({
                inputRange: [index - 1, index, index + 1],
                outputRange: [initWidth, 0, -initWidth],
            })

            return {
                transform: [{translateX}],
            }
        }
    }
})

export default createBottomTabNavigator(
    {
        Home: {
            screen: createStackNavigator({
                    Home: {
                        screen: HomeScreen,
                    },
                    Catalog: {
                        screen: CatalogScreen
                    },
                    CatalogSort: {
                        screen: CatalogSortScreen,
                    },
                    CatalogSortItem: {
                        screen: CatalogSortItemScreen,
                    },
                    Product: {
                        screen: ProductScreen
                    },
                },
                {transitionConfig: TransitionConfig}
            ),
        },
        Favorites: {
            screen: createStackNavigator({
                Favorites: {
                    screen: FavoritesScreen,
                    navigationOptions: {
                        title: 'Избранное'
                    },
                },
            }),
        },
        Cart: {
            screen: createStackNavigator({
                Cart: {
                    screen: CartScreen,
                    navigationOptions: {
                        title: 'Корзина'
                    },
                },
                CartDelivery: {
                    screen: CartDeliveryScreen,
                },
                CartDeliveryInfo: {
                    screen: CartDeliveryInfoScreen,
                },
                CartPayments: {
                    screen: CartPaymentsScreen,
                },
                CartInformation: {
                    screen: CartInformationScreen,
                },
            }),
        },
        User: {
            screen: createStackNavigator({
                    AuthLoading: {
                        screen: AuthLoadingScreen,
                        navigationOptions: {
                            header: null
                        },
                    },
                    Authorization: {
                        screen: AuthorizationScreen,
                        navigationOptions: {
                            title: 'Авторизация'
                        },
                    },
                    ConfirmRegistration: {
                        screen: ConfirmRegistrationScreen,
                        navigationOptions: {
                            title: 'Аутентификация'
                        },
                    },
                    Profile: {
                        screen: ProfileScreen,
                        navigationOptions: {
                            title: 'Профиль'
                        },
                    },
                    UserInfo: {
                        screen: UserInfoScreen,
                        navigationOptions: {
                            title: 'Информация'
                        },
                    },
                    BonusAccount: {
                        screen: BonusAccountScreen,
                        navigationOptions: {
                            title: 'Бонусы'
                        },
                    },
                    ChangePassword: {
                        screen: ChangePasswordScreen
                    },
                    Regestration: {
                        screen: RegestrationScreen,
                        navigationOptions: {
                            title: 'Регистрация'
                        },
                    },
                    VirtualCard: {
                        screen: VirtualCardScreen,
                        navigationOptions: {
                            title: 'Виртуальная карта'
                        },
                    },
                    History: {
                        screen: HistoryScreen,
                        navigationOptions: {
                            title: 'История заказов'
                        },
                    },
                },
                {transitionConfig: TransitionConfig}
            ),
        },
    },
    {
        defaultNavigationOptions: ({navigation, screenProps}) => ({
            tabBarIcon: ({focused}) => {
                const {routeName} = navigation.state;
                let iconSource;

                switch (routeName) {
                    case 'Home':
                        iconSource = iconHome;
                        title = 'Каталог';
                        badge = 0;
                        break;
                    case 'Favorites':
                        iconSource = iconFavorites;
                        title = 'Избранное';
                        badge = screenProps.countFavourites;
                        break;
                    case 'Cart':
                        iconSource = iconCart;
                        title = 'Корзина';
                        badge = screenProps.countCart;
                        break;
                    case 'User':
                        iconSource = iconUser;
                        title = 'Профиль';
                        badge = 0;
                        break;
                    default:
                        iconSource = iconHome;
                }
                return (
                    <View style={styles.tabBarItemContainer}>
                        <Image
                            resizeMode="contain"
                            source={iconSource}
                            style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
                        />
                        {
                            badge ? <View style={styles.badge}><Text style={styles.badgeText}>{badge}</Text></View> :
                                <View></View>
                        }
                        <Text style={styles.tabBarTitle}>{title}</Text>
                    </View>
                );
            },
        }),
        tabBarPosition: 'bottom',
        animationEnabled: true,
        swipeEnabled: true,
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            style: {
                backgroundColor: '#FFFFFF',
            },
            labelStyle: {
                color: '#7C7C7C',
                fontSize: 10,
                lineHeight: 12,
                padding: 0,
                margin: 0,
                fontWeight: '300'
            },
        },
        navigationOptions: {
            headerBackTitle: null
        }
    },
);
