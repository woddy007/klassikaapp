import React, { Component } from 'react';

// import AuthScreen from '../containers/AuthScreen';
import AppNavigator from './RootNavigation';
import {compose, lifecycle} from "recompose";
import {connect} from "react-redux";
import {getCountCart} from "../cart/cart/CartViewState";
import { getCountFavourites } from '../favorites/favorites/FavoritesViewState';

class NavigatorView extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        let params = {
            countCart: getCountCart(),
            countFavourites: getCountFavourites()
        }
        return(
            <AppNavigator screenProps={params}/>
        )
    }
}

export default compose(
    connect(
        state => ({
            cart: state.cart,
            favorites: state.favorites,
        }),
    ),
)(NavigatorView);
