import React from 'react';
import {Image, TouchableOpacity, Text, Platform} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import MainTabNavigator from './MainTabNavigator';
import Error401Screen from '../error/401/Error401ViewContainer'
import Error404Screen from '../error/404/Error404ViewContainer'

const stackNavigator = createStackNavigator(
    {
        Main: {
            screen: MainTabNavigator,
        },
        Error401: {
            screen: Error401Screen
        },
        Error404: {
            screen: Error404Screen
        }
    },
    {
        defaultNavigationOptions: () => ({
            headerStyle: {
                borderBottomWidth: 0,
                elevation: 0,
                shadowOpacity: 0,
            },
            headerTitleStyle: {
                color: '#000000',
                fontWeight: '500',
                fontSize: 16
            },
            headerTintColor: '#222222',
            headerLeftContainerStyle: {
                padding: 16
            },
            headerRightContainerStyle: {
                padding: 16
            },
            headerTitleStyle: {
                alignSelf: (Platform.OS === 'android') ? 'flex-end' : 'center'
            },
        }),
        headerMode: 'none',
    },
);

export default createAppContainer(stackNavigator);
