import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    ActivityIndicator,
    Alert,
    Platform, AsyncStorage,
} from 'react-native';

import axios from "../../../plugins/axios";
import colors from "../../../styles/colors";
import * as SecureStore from "expo-secure-store";

class AuthLoading extends Component {
    componentDidMount() {
        var navigation = this.props.navigation

        if (this.props.user.user) {
            navigation.replace({
                routeName: 'Profile'
            })
        } else {
            this._checkAuthorizationAsync()
        }
    }

    _checkAuthorizationAsync = () => {
        var navigation = this.props.navigation

        axios('get', 'api-shop-shopapp/me').then(response => {
            let user = response.data
            this.props.setUser(user)

            console.log('response: ', response)

            navigation.replace({
                routeName: 'Profile'
            })
        }).catch(error => {
            console.log('error: ', error.response)

            navigation.replace({
                routeName: 'Authorization'
            })
        })
    };

    render() {
        return (
            <View style={styles.page}>
                <ActivityIndicator
                    size={(Platform.OS != 'ios') ? 100 : 'large'}
                    color={colors.primary}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        padding: 14,
        backgroundColor: '#F3F5F7',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

export default AuthLoading
