import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import AuthLoading from './AuthLoadingView';
import { setUser } from '../user_info/UserInfoViewState'

export default compose(
    connect(
        state => ({
            user: state.user
        }),
        dispatch => ({
            setUser: (user) => dispatch(setUser(user))
        }),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(AuthLoading);
