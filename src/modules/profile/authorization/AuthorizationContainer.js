import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Authorization from './AuthorizationView';
import { sendPhone } from './AuthorizationState'


export default compose(
    connect(
        state => ({}),
        dispatch => ({
            sendPhone: (phone) => sendPhone(phone)
        }),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(Authorization);
