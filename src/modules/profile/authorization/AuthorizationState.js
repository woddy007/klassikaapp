import axios from "../../../plugins/axios";

const initialState = {

};

export function sendPhone(phone) {
    let data = {
        phone
    }

    return axios('post', 'test', data)
}

// Reducer
export default function HomeState(state = initialState, action = {}) {
    switch (action.type) {
        default:
            return state;
    }
}
