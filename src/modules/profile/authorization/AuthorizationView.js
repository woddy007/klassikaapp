import React, {Component} from 'react'
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Platform,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    Modal,
    ActivityIndicator
} from 'react-native';
import {TextInputMask} from "react-native-masked-text";
import axios from "../../../plugins/axios";
import Form from "../../../components/Form";
import InputText from '../../../components/InputText'
import Button from "../../../components/Button";
import common from "../../../styles/common";
import colors from "../../../styles/colors";
import {DropDownHolder} from "../../../components/DropDownAlert";


class Authorization extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            showContentRegestration: false,
            showModalLoader: false
        }
    }
    isLogin = () => {
        let phoneNotFormat = this.state.phone

        if (phoneNotFormat.length == 16) {
            var navigation = this.props.navigation
            let phoneFormat = parseInt(phoneNotFormat.replace(/\D+/g,""));
            phoneFormat = '' + phoneFormat

            this.setState({showModalLoader: true})

            let body = new FormData()
            body.append(
                'PasswordResetRequestForm[phone]', phoneFormat
            )

            axios('post', 'api-shop-shopapp/request', body).then(response => {
                this.setState({showModalLoader: false})

                navigation.navigate({
                    routeName: 'ConfirmRegistration',
                    params: {
                        phone: phoneFormat
                    }
                })
            }).catch(error => {
                this.setState({showModalLoader: false})

                navigation.navigate({
                    routeName: 'Regestration',
                    params: {
                        phone: this.state.phone
                    }
                })
            })
        } else {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не все поля заполнены');
        }
    }

    render() {
        return (
            <View style={common.pageStyle}>
                <ScrollView>
                    <View>
                        <Text style={styles.description}>
                            Войдите или зарегистрируйтесь для отслеживания заказов и участия в специальных акциях
                        </Text>
                    </View>
                    <View style={styles.form}>
                        <TextInputMask
                            type={'custom'}
                            options={{
                                mask: '+7(999)999-99-99',
                            }}
                            value={this.state.phone}
                            onChangeText={phone => this.setState({phone})}
                            style={styles.formLine}
                            keyboardType="number-pad"
                            placeholder="+7(999)999-99-99"
                        />
                        {
                            (this.state.showContentRegestration) ? (
                                <View style={styles.contentRegestration}>
                                    <Text>Пользователь с таким номером телефона не найден</Text>
                                </View>
                            ) : <View/>
                        }

                    </View>

                    <View style={styles.contentButtons}>
                        <View style={styles.contentButton}>
                            <Button
                                label='ВЫСЛАТЬ КОД ПОДТВЕРЖДЕНИЯ'
                                type='secondary'
                                click={() => this.isLogin()}
                            />
                        </View>
                    </View>
                </ScrollView>


                <Modal
                    visible={this.state.showModalLoader}
                >
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator
                            size={(Platform.OS != 'ios') ? 100 : 'large'}
                            color={colors.primary}
                        />
                    </View>
                </Modal>

            </View>
        )
    }

    static navigationOptions = {
        title: 'Details',
    };
}

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        marginBottom: 8,
        fontWeight: '500'
    },
    description: {
        marginBottom: 10,
        color: '#7C7C7C'
    },

    form: {
        marginBottom: 20
    },
    formLine: {
        marginBottom: 17,
        height: 35,
        width: '100%',
        backgroundColor: 'white',
        borderWidth: 0.5,
        paddingHorizontal: 15,
        borderStyle: 'solid',
        borderColor: '#CDCDCD'
    },
    contentButtons: {
        marginBottom: 15
    },
    contentButton: {
        marginBottom: 12
    },
    privacyPolicy: {
        alignItems: 'center',
    },

    contentRegestration: {
        flexDirection: 'row',
    },
    textRegestration: {},
    buttonRegestration: {},
})

export default Authorization
