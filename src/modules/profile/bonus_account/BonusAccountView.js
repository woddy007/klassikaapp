import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Platform,
    Modal, Alert,
} from 'react-native';
import axios from "../../../plugins/axios";
import colors from "../../../styles/colors";
import {DropDownHolder} from "../../../components/DropDownAlert";

class BonusAccount extends Component {
    constructor(props) {
        super(props)

        this.state = {
            listHistory: [],
            totalBonus: null
        }
    }

    componentDidMount = () => {
        this.loadBonus()
    }
    loadBonus = () => {
        axios('get', 'api-shop-shopapp/bonus').then(response => {
            let data = response.data
            let bonus = data.total_bonus
            let history = data.bonus_list

            this.setState({
                totalBonus: data.total_bonus,
                listHistory: data.bonus_list,
            })

        }).catch(error => {
            let errorText = 'Ошибка сервера'

            if ( error.response.data.message ){
                errorText = error.response.data.message
            }

            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', errorText);
        })
    }
    _getData = (timestamp) => {
        let date = new Date(timestamp * 1000)

        var options = {
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
        };

        return date.toLocaleString("ru", options)
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.blockCountBonus}>
                    <Text style={styles.blockCountLabel}>Мой баланс</Text>
                    <Text style={styles.blockCountValue}>{(this.state.totalBonus)? this.state.totalBonus: 0} баллов</Text>
                </View>

                <ScrollView style={styles.scrollView}>
                    {
                        this.state.listHistory.map((item, idx) => {
                            return (
                                <TouchableOpacity
                                    key={idx}
                                    style={styles.orderBlock}
                                >
                                    <View style={styles.orderTopLine}>
                                        {
                                            (item.type != 2) ?
                                                <Text style={styles.orderNumber}>Заказ №{item.order_id}</Text> :
                                                <View style={{height: 0}}/>
                                        }
                                        <Text
                                            style={styles.orderBonus}>{(item.type == 2) ? '' : '+'}{item.value} Б</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.orderDate}>{this._getData(item.created_at)}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    }
                </ScrollView>
            </View>
        )
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Григоий'
        };
    };
}

const
    styles = StyleSheet.create({
        page: {
            padding: 14,
            flex: 1,
            backgroundColor: '#F3F5F7'
        },

        blockCountBonus: {
            backgroundColor: 'white',
            paddingHorizontal: 10,
            paddingVertical: 16,
            marginBottom: 15
        },
        blockCountLabel: {
            fontSize: 14,
            color: '#7C7C7C',
        },
        blockCountValue: {
            fontSize: 18,
            fontWeight: 'bold',
        },

        scrollView: {},
        orderBlock: {
            padding: 10,
            borderBottomWidth: 0.5,
            borderStyle: 'solid',
            borderColor: '#CDCDCD',
            backgroundColor: 'white',
        },
        orderTopLine: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 3
        },
        orderNumber: {
            fontSize: 16,
        },
        orderDate: {
            fontSize: 14,
            color: '#7C7C7C',
            fontWeight: '300'
        },
        orderBonus: {
            fontSize: 16,
            fontWeight: '500'
        }
    })

export default BonusAccount
