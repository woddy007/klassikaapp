import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import BonusAccount from './BonusAccountView';

export default compose(
    connect(
        state => ({}),
        dispatch => ({}),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(BonusAccount);
