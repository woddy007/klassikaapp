import axios from "../../../plugins/axios";

const WRITE_LIST = 'bonus/WRITE_LIST'
const WRITE_BONUS = 'bonus/WRITE_BONUS'

const initialState = {
    history: [],
    bonuses: 0
};

export function wrileList(list) {
    let history = initialState.history

    history = list

    return {
        type: WRITE_LIST,
        history
    }
}
export function wrileBonuses(bonus) {
    let bonuses = initialState.bonuses

    bonuses = bonus

    return {
        type: WRITE_BONUS,
        bonuses
    }
}

// Reducer
export default function BonusAccount(state = initialState, action = {}) {
    switch (action.type) {
        case WRITE_LIST: {
            let history = action.history

            return {
                ...state,
                history
            }
        }
        case WRITE_BONUS: {
            let bonuses = action.bonuses

            return {
                ...state,
                bonuses
            }
        }

        default:
            return state;
    }
}
