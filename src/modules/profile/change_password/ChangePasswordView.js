import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import Button from "../../../components/Button";
import Form from "../../../components/Form";
import InputText from "../../../components/InputText";

class ChangePassword extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        return (
            <View style={styles.page}>
                <ScrollView style={styles.scrollView}>
                        <InputText
                            label="Старый пароль"
                            value="Григорий"
                            type="password"
                            filled
                            classContent={styles.input}
                        />
                        <InputText
                            label="Фамилия"
                            type="password"
                            filled
                            classContent={styles.input}
                        />
                        <InputText
                            label="Отчество"
                            type="password"
                            filled
                            classContent={styles.input}
                        />
                </ScrollView>

                <View style={styles.buttonContent}>
                    <Button
                        label="Сохранить"
                        type="secondary"
                    />
                </View>
            </View>
        )
    }

    static navigationOptions = ({navigation}) => {
        return {};
    };
}


const styles = StyleSheet.create({
    page: {
        padding: 14,
        backgroundColor: '#F3F5F7',
        flex: 1,
    },
    scrollView: {
        marginBottom: 25
    },
    buttonContent: {},
    input: {
        marginBottom: 8
    }
})

export default ChangePassword
