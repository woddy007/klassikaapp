import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import ChangePassword from './ChangePasswordView';

export default compose(
    connect(
        state => ({}),
        dispatch => ({}),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(ChangePassword);
