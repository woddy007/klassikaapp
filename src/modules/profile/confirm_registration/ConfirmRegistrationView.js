import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    Alert, AsyncStorage, ActivityIndicator, Platform, Modal
} from 'react-native';
import Loading from "../../../components/Loading";
import HomeHeaderSearch from "../../../components/HomeHeaderSearch";
import common from "../../../styles/common";
import Form from "../../../components/Form";
import InputText from "../../../components/InputText";
import Button from "../../../components/Button";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import * as SecureStore from 'expo-secure-store';
import colors from "../../../styles/colors";

class ConfirmRegistration extends Component {
    constructor(props) {
        super(props)

        this.state = {
            phone: this.props.navigation.state.params.phone,
            code: null,
            showModalLoader: false
        }
    }

    componentDidMount() {
        let params = this.props.navigation.state.params

        this.setState({
            phone: params.phone
        })
    }

    setToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {
            // Error saving data
        }
    }
    isLogin = () => {
        this.setState({ showModalLoader: true })

        if (this.refs['form-login'].checkForm()) {
            let body = new FormData()
            body.append(
                'username', this.state.phone
            )
            body.append(
                'password', this.state.code
            )

            axios('post', 'api-shop-shopapp/login', body).then(response => {
                this.setToken(response.data.jwt_token)
                this.props.navigation.replace({
                    routeName: 'AuthLoading'
                })
            }).catch(error => {
                console.log('error: ', error.response)

                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не правильно введен код');
            })

            this.setState({ showModalLoader: false })
        } else {
            this.setState({ showModalLoader: false })
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не все поля заполнены');
        }
    }

    render() {
        return (
            <View style={common.pageStyle}>
                <Text style={styles.description}>
                    Пожалуйста, введите код из СМС
                </Text>


                <View style={styles.form}>
                    <Form ref="form-login">
                            <InputText
                                value={this.state.code}
                                label='Код из СМС'
                                onChange={code => this.setState({code})}
                                rules={val => val && val.length > 0 || 'Заполните поле'}
                            />
                    </Form>
                </View>

                <Button
                    label='ВОЙТИ'
                    type='secondary'
                    click={this.isLogin}
                />

                <Modal
                    visible={this.state.showModalLoader}
                    transparent
                >
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(243, 245, 247, 0.8)'}}>
                        <ActivityIndicator
                            size={(Platform.OS != 'ios') ? 100 : 'large'}
                            color={colors.primary}
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
        label: {
            fontSize: 16,
            marginBottom: 8,
            fontWeight: '500'
        },
        description: {
            marginBottom: 10,
            color: '#7C7C7C'
        },
        form: {
            marginBottom: 20
        },
    })

export default ConfirmRegistration
