// @flow
type HistoryStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: HistoryStateType = {};

export const ACTION = 'HistoryState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function HistoryStateReducer(state: HistoryStateType = initialState, action: ActionType): HistoryStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
