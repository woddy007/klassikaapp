import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    SafeAreaView,
    FlatList, ActivityIndicator
} from 'react-native';
import axios from "../../../plugins/axios";
import colors from "../../../styles/colors";
import dateConvert from '../../../helper/date'


class History extends Component {
    constructor(props) {
        super(props);

        this.state = {
            historyList: [],
            loading: true
        }
    }

    componentDidMount = () => {
        this.loadHistoryList()
    }

    loadHistoryList = () => {
        axios('get', 'api-shop-shopapp/history').then(response => {
            this.setState({
                historyList: response.data,
                loading: false
            })
        }).catch(error => {
            this.setState({
                loading: false
            })
        })
    }

    itemHistory = (item) => {
        console.log('item: ', item)

        return (
            <View style={styles.card}>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={{ fontWeight: '500',fontSize: 18 }}>Номер заказа: </Text>
                    <Text style={{ fontSize: 18 }}>{item.id}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Text style={{ fontWeight: '500',fontSize: 18 }}>Дата: </Text>
                    <Text style={{ fontSize: 18 }}>{dateConvert(item.created_at * 1000, "dd-mm-yyyy HH:mm")}</Text>
                </View>
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontWeight: '500' }}>Стоимость заказа: </Text>
                        <Text>{item.cost}₽</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontWeight: '500' }}>Статус: </Text>
                        <Text>{ item.status_name }</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontWeight: '500' }}>Способ оплаты: </Text>
                        <Text>{ item.peyment_method_name }</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontWeight: '500' }}>Способ доставки: </Text>
                        <Text>{ item.delivery_method_name_ru }</Text>
                    </View>
                </View>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.page}>
                {
                    (this.state.historyList.length > 0)
                        ?
                        <FlatList
                            data={this.state.historyList}
                            renderItem={({item}) => this.itemHistory(item)}
                            keyExtractor={item => 'history-' + item.id}
                        />
                        :
                        <View>
                            {
                                (this.state.loading)
                                    ?
                                    <ActivityIndicator
                                        size="large"
                                        color="#8F7E5A"
                                    />
                                    :
                                    <View style={styles.listEmpty}>
                                        <Text>история пуста</Text>
                                    </View>
                            }
                        </View>
                }
            </SafeAreaView>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'История заказов',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: '#F3F5F7'
    },

    card: {
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: 'black',
        padding: 10
    },
    cardTop: {
        flexDirection: 'row'
    },
    cardName: {
        marginRight: 15,
        fontSize: 16
    },
    cardPhone: {
        fontSize: 16
    },
    listEmpty: {
        padding: 20
    }
})

export default History
