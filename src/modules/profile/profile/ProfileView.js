import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Platform,
    AsyncStorage
} from 'react-native';
import axios from "../../../plugins/axios";

const iconArrow = require('../../../../assets/icons/next-right.png');

const iconProfile = require('../../../../assets/images/tabbar/user.png');
const iconShop = require('../../../../assets/images/tabbar/shopping-cart.png');
const iconLock = require('../../../../assets/icons/lock.png');
const iconTag = require('../../../../assets/icons/tag.png');
const iconHistory = require('../../../../assets/icons/history.png');
const iconDiscountCard = require('../../../../assets/icons/discount-card.png');

class Profile extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Григорий'
        };
    };

    constructor(props) {
        super(props)

        this.state = {
            isFocused: false
        }
    }

    userExit = async () => {
        await AsyncStorage.removeItem('token')
        const {navigate} = this.props.navigation
        this.props.userExit()
        console.log('token: ', await AsyncStorage.getItem('token'))
        this.props.navigation.replace('Authorization');
    }

    render() {
        const { navigate } = this.props.navigation

        return (
            <View style={styles.page}>
                <ScrollView>
                    <View>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigate('UserInfo')}
                        >
                            <View style={styles.buttonIconContent}>
                                <Image
                                    style={styles.buttonIcon}
                                    source={iconProfile}
                                />
                            </View>
                            <View style={styles.buttonContent}>
                                <View>
                                    <Text style={styles.buttonlabel}>Мой профиль</Text>
                                    <Text style={styles.buttonHint}>Изменить данные</Text>
                                </View>
                                <View style={styles.buttonRightContent}>
                                    <Image
                                        style={styles.buttonIcon}
                                        source={iconArrow}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginVertical: 15 }}>
                        <TouchableOpacity
                            style={[styles.button, styles.buttonBottomLine]}
                            onPress={() => navigate('BonusAccount')}
                        >
                            <View style={styles.buttonIconContent}>
                                <Image
                                    style={styles.buttonIcon}
                                    source={iconTag}
                                />
                            </View>
                            <View style={styles.buttonContent}>
                                <View>
                                    <Text style={styles.buttonlabel}>Бонусный счет</Text>
                                </View>
                                <View style={styles.buttonRightContent}>
                                    {
                                        (false)? <Text style={styles.buttonPoints}>10 Б</Text>: <View/>
                                    }
                                    <Image
                                        style={styles.buttonIcon}
                                        source={iconArrow}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.button, styles.buttonBottomLine]}
                            onPress={() => navigate('History')}
                        >
                            <View style={styles.buttonIconContent}>
                                <Image
                                    style={styles.buttonIcon}
                                    source={iconHistory}
                                />
                            </View>
                            <View style={styles.buttonContent}>
                                <View>
                                    <Text style={styles.buttonlabel}>История заказов</Text>
                                </View>
                                <View style={styles.buttonRightContent}>
                                    <Image
                                        style={styles.buttonIcon}
                                        source={iconArrow}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigate('VirtualCard')}
                        >
                            <View style={styles.buttonIconContent}>
                                <Image
                                    style={styles.buttonIcon}
                                    source={iconDiscountCard}
                                />
                            </View>
                            <View style={styles.buttonContent}>
                                <View>
                                    <Text style={styles.buttonlabel}>Виртуальная карта</Text>
                                </View>
                                <View style={styles.buttonRightContent}>
                                    <Image
                                        style={styles.buttonIcon}
                                        source={iconArrow}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{marginTop: 15}}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.userExit()}
                        >
                            <View style={styles.buttonContent}>
                                <View>
                                    <Text style={[styles.buttonlabel, { color: '#EF5252' }]}>Выйти</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        paddingVertical: 14,
        backgroundColor: '#F3F5F7',
        flex: 1
    },
    button: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'white',
        paddingVertical: 16,
        paddingHorizontal: 20,
        minHeight: 65
    },
    buttonBottomLine: {
        borderBottomWidth: 0.5,
        borderColor: '#CDCDCD',
        borderStyle: 'solid'
    },
    buttonContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1
    },
    buttonIconContent: {
        marginRight: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonIcon: {
        width: 20,
        height: 20,
    },
    buttonlabel: {
        fontSize: 14,
    },
    buttonHint: {
        fontSize: 10,
        color: '#7C7C7C',
    },
    buttonRightContent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonPoints: {
        fontSize: 14,
        color: '#7C7C7C',
        marginRight: 8
    },
    buttonRightArrow: {
        width: 10,
        height: 15
    },
})

export default Profile


// <TouchableOpacity
// style={[styles.button, styles.buttonBottomLine]}
// onPress={() => navigate('ChangePassword')}
// >
// <View style={styles.buttonIconContent}>
//     <Image
// style={styles.buttonIcon}
// source={iconLock}
// />
// </View>
// <View style={styles.buttonContent}>
// <View>
// <Text style={styles.buttonlabel}>Изменить пароль</Text>
// </View>
// <View style={styles.buttonRightContent}>
// <Image
// style={styles.buttonIcon}
// source={iconArrow}
// />
// </View>
// </View>
// </TouchableOpacity>

// <TouchableOpacity
// style={[styles.button, styles.buttonBottomLine]}
// >
// <View style={styles.buttonIconContent}>
//     <Image
// style={styles.buttonIcon}
// source={iconShop}
// />
// </View>
// <View style={styles.buttonContent}>
// <View>
// <Text style={styles.buttonlabel}>Мои заказы</Text>
// <Text style={styles.buttonHint}>Нет заказов</Text>
// </View>
// <View style={styles.buttonRightContent}>
// <Image
// style={styles.buttonIcon}
// source={iconArrow}
// />
// </View>
// </View>
// </TouchableOpacity>

// <TouchableOpacity
// style={styles.button}
//     >
//     <View style={styles.buttonContent}>
//     <View>
//     <Text style={styles.buttonlabel}>Наши магазины</Text>
// </View>
// <View style={styles.buttonRightContent}>
// <Image
// style={styles.buttonIcon}
// source={iconArrow}
// />
// </View>
// </View>
// </TouchableOpacity>
