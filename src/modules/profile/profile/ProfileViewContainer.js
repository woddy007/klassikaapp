import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import Profile from './ProfileView.js';
import { userExit } from '../user_info/UserInfoViewState'

export default compose(
    connect(
        state => ({}),
        dispatch => ({
            userExit: () => dispatch(userExit())
        }),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(Profile);
