import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Alert,
    Modal,
    ActivityIndicator,
    Platform
} from 'react-native';
import Form from "../../../components/Form";
import InputText from "../../../components/InputText";
import Button from "../../../components/Button";
import common from "../../../styles/common";
import axios from "../../../plugins/axios";
import colors from "../../../styles/colors";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {TextInputMask} from "react-native-masked-text";

class Regestration extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: null,
            phone: this.props.navigation.state.params.phone,
            showModalLoader: false
        }
    }

    isRegestrition = () => {
        let validation = true
        let phoneNotFormat = this.state.phone

        if ( phoneNotFormat.length != 16 ){
            validation = false
        }

        if ( !this.state.name ){
            validation = false
        }

        if (validation) {
            this.setState({showModalLoader: true})
            const {navigate} = this.props.navigation

            let phoneFormat = parseInt(phoneNotFormat.replace(/\D+/g,""));
            phoneFormat = '' + phoneFormat

            let body = new FormData()
            body.append(
                'SignupForm[name]', this.state.name
            )
            body.append(
                'SignupForm[phone]', phoneFormat
            )

            console.log('body: ', body)

            axios('post', 'api-shop-shopapp/signup', body).then(response => {
                this.setState({showModalLoader: false})

                navigate('ConfirmRegistration', {
                    phone: phoneFormat
                })
            }).catch(error => {
                this.setState({showModalLoader: false})
                let errorText = 'Ошибка сервера'

                if (error.response.data.message) {
                    errorText = error.response.data.message
                }

                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', errorText);
            })
        } else {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не все поля заполнены');
        }
    }

    render() {
        const {navigate} = this.props.navigation

        return (
            <View style={common.pageStyle}>
                <ScrollView>
                    <View>
                        <Text style={styles.description}>
                            Пожалуйста, заполните поля формы для регистрации:
                        </Text>
                    </View>
                    <View style={styles.form}>
                        <InputText
                            value={this.state.name}
                            label='Ваше имя'
                            onChange={name => this.setState({name})}
                            classContent={styles.formLine}
                        />
                        <TextInputMask
                            type={'custom'}
                            options={{
                                mask: '+7(999)999-99-99',
                            }}
                            value={this.state.phone}
                            onChangeText={phone => this.setState({phone})}
                            style={styles.textInputMask}
                            keyboardType="number-pad"
                            placeholder="+7(999)999-99-99"
                        />
                    </View>
                    <View style={styles.contentButtons}>
                        <View style={styles.contentButton}>
                            <Button
                                label='РЕГИСТРАЦИЯ'
                                type='secondary'
                                click={() => this.isRegestrition()}
                            />
                        </View>
                    </View>
                </ScrollView>

                <Modal
                    visible={this.state.showModalLoader}
                >
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator
                            color={colors.primary}
                            size={(Platform.OS != 'ios') ? 100 : 'large'}
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        marginBottom: 8,
        fontWeight: '500'
    },
    description: {
        marginBottom: 10,
        color: '#7C7C7C'
    },

    form: {
        marginBottom: 20
    },
    formLine: {
        marginBottom: 17
    },
    contentButtons: {
        marginBottom: 15
    },
    contentButton: {
        marginBottom: 12
    },
    privacyPolicy: {
        alignItems: 'center',
    },

    textInputMask: {
        marginBottom: 17,
        height: 35,
        width: '100%',
        backgroundColor: 'white',
        borderWidth: 0.5,
        paddingHorizontal: 15,
        borderStyle: 'solid',
        borderColor: '#CDCDCD'
    },
})

export default Regestration
