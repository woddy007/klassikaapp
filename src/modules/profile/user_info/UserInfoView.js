import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
    Platform,
    Modal,
    Alert
} from 'react-native';
import Button from "../../../components/Button";
import Form from "../../../components/Form";
import InputText from "../../../components/InputText";
import colors from "../../../styles/colors";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";

class UserInfo extends Component {
    constructor(props) {
        super(props)


        this.state = {
            showModalLoader: false,
            user: this.props.user.user
        }
    }

    componentDidMount = () => {

    }
    save = () => {
        this.setState({showModalLoader: true})

        setTimeout(() => {
            this.setState({showModalLoader: false})
            DropDownHolder.dropDown.alertWithType('info', 'Разработка', 'Метод находиться в разработке');
        }, 2000)
    }

    render() {
        let user = this.state.user

        return (
            <View style={styles.page}>
                <ScrollView style={styles.scrollView}>
                        <InputText
                            label="Имя"
                            value={user.name}
                            filled
                            classContent={styles.input}
                            onChange={name => {
                                this.setState((prevState) => ({
                                    user: {
                                        ...prevState.user,
                                        name: name
                                    }
                                })
                            )}}
                        />
                        <InputText
                            label="E-mail"
                            value={user.email}
                            filled
                            classContent={styles.input}
                            onChange={email => {
                                this.setState((prevState, email) => ({
                                    user: {
                                        ...prevState.user,
                                        email: email
                                    }
                                })
                            )}}
                        />
                        <InputText
                            label="Мобильный телефон"
                            value={user.phone}
                            filled
                            classContent={styles.input}

                            disabled
                        />
                </ScrollView>
                <View style={styles.buttonContent}>
                    <Button
                        label="Сохранить"
                        type="secondary"
                        click={() => this.save()}
                    />
                </View>

                <Modal
                    visible={this.state.showModalLoader}
                    transparent
                >
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(243, 245, 247, 0.8)'}}>
                        <ActivityIndicator
                            size={(Platform.OS != 'ios') ? 100 : 'large'}
                            color={colors.primary}
                        />
                    </View>
                </Modal>
            </View>
        )
    }

    static navigationOptions = ({navigation}) => {
        return {};
    };
}


const styles = StyleSheet.create({
    page: {
        padding: 14,
        backgroundColor: '#F3F5F7',
        flex: 1,
    },
    scrollView: {
        marginBottom: 25
    },
    buttonContent: {},
    input: {
        marginBottom: 8
    }
})

export default UserInfo
