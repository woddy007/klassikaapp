import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import UserInfo from './UserInfoView';

export default compose(
    connect(
        state => ({
            user: state.user
        }),
        dispatch => ({}),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(UserInfo);
