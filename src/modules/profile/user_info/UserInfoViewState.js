import axios from "../../../plugins/axios";

const SET_USER = 'userInfo/SET_USER'
const USER_EXIT = 'userInfo/USER_EXIT'

const initialState = {
    user: null
};

export function setUser(user) {
    return {
        type: SET_USER,
        user
    }
}

export function userExit() {
    let user = initialState.user

    user = null

    return {
        type: USER_EXIT,
        user
    }
}

// Reducer
export default function UserInfo(state = initialState, action = {}) {
    switch (action.type) {
        case SET_USER:{
            let user = action.user

            return {
                ...state,
                user: user
            }
        }
        case USER_EXIT:{
            let user = action.user

            return {
                ...state,
                user
            }
        }
        default:
            return state;
    }
}
