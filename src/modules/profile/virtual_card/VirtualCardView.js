import React, {Component} from 'react'
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    Image,
} from 'react-native';
import * as Brightness from 'expo-brightness';
import * as Permissions from 'expo-permissions';

const imageCard = require('../../../../assets/images/VirtualCard.png');

class VirtualCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            brighness: 0
        }
    }

    componentDidMount = async () => {
        const {status} = await Permissions.askAsync(Permissions.SYSTEM_BRIGHTNESS);

        if (status === 'granted') {
            let currentBrighness = await Brightness.getBrightnessAsync()

            this.setState({
                brighness: currentBrighness
            })

            Brightness.setSystemBrightnessAsync(1);
        }
    };
    componentWillUnmount = async () => {
        const {status} = await Permissions.askAsync(Permissions.SYSTEM_BRIGHTNESS);

        if (status === 'granted') {
            Brightness.setSystemBrightnessAsync(this.state.brighness);
        }
    }


    getImageBase64 = () => {
        const base64 = 'data:image/png;base64,'
        return base64 + 'iVBORw0KGgoAAAANSUhEUgAAAMoAAAAeAQMAAABXBBPSAAAABlBMVEX///8AAABVwtN+AAAAAXRSTlMAQObYZgAAACxJREFUKJFj+Mx/mPkwz4c/Nh9sDh8wZj5w/rOBvfGBD0DMMCo1KjUqBZMCAPummfxHmWQ8AAAAAElFTkSuQmCC'
    }

    render() {
        const {navigate} = this.props.navigation

        return (
            <View style={styles.page}>
                <Image
                    resizeMode="contain"
                    style={styles.virtualCard}
                    source={imageCard}
                />
                <Image
                    resizeMode="contain"
                    style={styles.barcode}
                    source={{uri: this.getImageBase64()}}
                />
                <Text style={styles.cardNumber}>80000000137569</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        padding: 25,
        backgroundColor: '#F3F5F7',
        alignItems: 'center',
    },
    virtualCard: {
        width: '100%',
        maxHeight: 200
    },
    barcode: {
        width: '100%',
        height: 75,
    },
    cardNumber: {
        fontSize: 16,
        fontWeight: '500'
    }
})

export default VirtualCard
