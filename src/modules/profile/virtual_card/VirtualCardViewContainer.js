import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import VirtualCard from './VirtualCardView';

export default compose(
    connect(
        state => ({}),
        dispatch => ({}),
    ),
    lifecycle({
        componentDidMount() {},
    }),
)(VirtualCard);
