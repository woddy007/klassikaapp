import axios from 'axios'
import varibles from '../varibles'
import {AsyncStorage} from "react-native";


export default async (type, url, data) => {
    let token = await AsyncStorage.getItem('token');

    let params = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }

    url = varibles.API_URL + url

    switch (type) {
        case 'get': {
            if (data) {
                params['data'] = data
            }

            return axios({
                    method: 'get',
                    url: url,
                    ...params
                }
            )
        }
        case 'post': {
            return axios({
                method: 'post',
                url: url,
                data: data,
                ...params
            })
        }
    }
}
