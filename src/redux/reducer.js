import { combineReducers } from 'redux';

import products from '../modules/catalog/catalog/CatalogViewState'
import user from '../modules/profile/user_info/UserInfoViewState'
import cart from '../modules/cart/cart/CartViewState'
import favorites from '../modules/favorites/favorites/FavoritesViewState'

const rootReducer = combineReducers({
    products,
    user,
    cart,
    favorites
});

export default rootReducer
