const common = {
    pageStyle: {
        padding: 15,
        backgroundColor: '#F3F5F7',
        flex: 1
    },

    blockFilter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

        paddingVertical: 21,
        paddingHorizontal: 16,
        borderBottomWidth: 0.5,
        borderColor: '#CDCDCD',
        borderStyle: 'solid',
        backgroundColor: 'white'
    },
    blockFilterLast: {
        borderBottomWidth: 0
    },
    blockFilterText: {
        color: '#7C7C7C',
        fontSize: 16
    },
    blockFilterIcon: {
        width: 15,
        height: 15,
        tintColor: '#7C7C7C'
    },
}

export default common
